$(document).ready(function () {
    $('select#to_email.error').closest('div.form-group').find('span.select2-selection.select2-selection--single').css('border', '1px solid red');

    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
        $('div#sidebar-content').show();
        $('div.alert').hide();
        $('div#edit-sidebar-content').hide();
        $('div#contact-member-content').hide();
        $('#sidebar').toggleClass('active');
        $('.overlay').toggleClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });

    $("form#edit-profile").validate({
        rules: {
            first_name: "required",
            last_name: "required"
        }
    });

    $("form#contact-member-form").validate({
        rules: {
            from_email: "required",
            subject_email: "required",
            body_email: {
                required: true,
                minlength: 50
            }
        }
    });

    $('#phone').usPhoneFormat({
        format: '(xxx) xxx-xxxx',
    });

    $( "#industry" ).select2({
        theme: "bootstrap4"
    });

    $( "#to_email" ).select2({
        theme: "bootstrap4",
        containerCssClass: "error",
        ajax: {
            url: window.base_url + 'user/get_members',
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    q: params.term // search term
                };
            },
            type: "GET",
            quietMillis: 50,
            processResults: function(data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.text,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        },
        minimumInputLength: 2
    });

});

$(document).on('click', 'button.edit-profile', function(){
    $('div#sidebar-content').hide();
    $('div#contact-member-content').hide();
    $('div#edit-sidebar-content').show();
});

$(document).on('click', 'button.contact-member', function(){
    $('div#sidebar-content').hide();
    $('div#edit-sidebar-content').hide();
    $('div#contact-member-content').show();
});

$(document).on('click', 'button.cancel_send_email', function(){
    $('#contact-member-form')[0].reset();
    $('#to_email').val(null).trigger('change');
    $('div.alert').hide();
    $('div#sidebar-content').show();
    $('div#edit-sidebar-content').hide();
    $('div#contact-member-content').hide();
});

$(document).on('click', 'button.clear_email', function(){
    $('#contact-member-form')[0].reset();
    $('#to_email').val(null).trigger('change');
    $('div.alert').hide();
});

$(document).on('click', 'button.send_email', function(){
    if($('select#to_email').val() == null){
        $('label.error').css('display', 'none');
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid red');
    } else {
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid #ced4da;')
    }
    if($("#contact-member-form").valid()){
        var contact_member_data = $("#contact-member-form").serializeArray();
        $.ajax({
            type: "POST", 
            url: window.base_url+'user/contact_member', 
            data: contact_member_data,
            dataType : 'JSON',
            success: function (response) {
                $('div.contact_member_alert').html('<div class="alert alert-' + response.alert_type + '" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.message + '</div>');
            },
                error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log("There was an error: " + errorThrown);  
            }
        });
    } else {
        $('label.error').css('display', 'none');
    }
});


$('select#to_email').on('select2:select', function(){
    if($('select#to_email').val() != null){
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid #ced4da');
    } else {
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid red');
    }
});

$(document).on('click', 'button.save_profile', function(){
    if($("#edit-profile").valid()){
        var profile_data = $("#edit-profile").serializeArray();

        $.ajax({
            type: "POST", 
            url: window.base_url+'user/update_profile', 
            data: profile_data,
            dataType : 'JSON',
            success: function (response) {
                $('div.update_profile_alert').html('<div class="alert alert-' + response.alert_type + '" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.message + '</div>');
            },
                error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log("There was an error: " + errorThrown);  
            }
        });
    }
});

$(document).on('click', 'button.cancel_profile', function(){
    $('div#sidebar-content').show();
    $('div.alert').hide();
    $('div#edit-sidebar-content').hide();
    $('div#contact-member-content').hide();
});