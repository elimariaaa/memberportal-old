var member = '';
var member_city = '';
var email_to = '';
var orig_content = '';

function imgError(image, gender, id) {
    image.onerror = "";
//    image.src = "/images/noimage.gif";
    if(gender == 'male') {
        image.src = 'assets/images/male.jpg';
        $('img.member-'+ id).data('linkedin_pic', 'assets/images/male.jpg');
    } else if(gender == 'female') {
        image.src = 'assets/images/female.png';
        $('img.member-'+ id).data('linkedin_pic', 'assets/images/female.png');
    } else {
        image.src = 'assets/images/random-profile.png';
        $('img.member-'+ id).data('linkedin_pic', 'assets/images/random-profile.png');
    }
    return true;
}

function imageExists(url, callback) {
    var img = new Image();
    img.onload = function() { callback(true); };
    img.onerror = function() { callback(false); };
    img.src = url;
}

$(document).ready(function(){
    $('.loadingDiv').hide();
    // Detect pagination click
    $('#pagination').on('click','a',function(e){
        e.preventDefault(); 
        var pageno = $(this).attr('data-ci-pagination-page');
        loadPagination(pageno, member, member_city);
    });

    loadPagination(0);
});

$(document).on('click', 'button.memberdir_refresh', function(){
    $('.loadingDiv').show();
    $('input.memberdir-val').val('');
    $('select#member_city').val('');
    member = '';
    member_city = '';
    loadPagination(0);
});

$(document).on('click', 'button.search-memberdir', function(){
    $('.loadingDiv').show();
    member = $('input.memberdir-val').val();
    if(member != ''){
        loadPagination(0, member, member_city);
    }
});

$(document).on('click', 'button.search-membercity', function(){
    $('.loadingDiv').show();
    member_city = $('select#member_city').val();
    if(member_city != ''){
        loadPagination(0, member, member_city);
    }
});

// Load pagination
function loadPagination(pagno, member, member_plan){
    $.ajax({
        url: window.base_url + 'member/mdirectory/loadRecord/'+pagno,
        type: 'get',
        data: {'member_data': member, 'member_city': member_city},
        dataType: 'json',
        success: function(response){
            $('.loadingDiv').hide();
            $('#pagination').html(response.pagination);
            createTable(response.result,response.row);
        }
    });
}

// Create table list
function createTable(result,sno){
    sno = Number(sno);
    var ctr = 0
    var tr = '';
    var ctr2 = 0;
    $('div.memberdirectory_holder').empty();
    if(result != ''){
        for(index in result){
            var id = result[index].id;
            var first_name = result[index].first_name;
            var last_name = result[index].last_name;
            var linkedin_pic = result[index].linkedin_pic;
            var occupation = result[index].position;
            var company = result[index].company;
            var industry = result[index].industry;
            var first_bill_date = result[index].first_bill_date;
            var gender = result[index].gender;
            var bio = result[index].bio;
            var email = result[index].email;
            var braintree_customer_id = result[index].braintree_customer_id;
    
            if(!linkedin_pic) {
                if(result[index].gender == 'male') {
                    linkedin_pic = 'assets/images/male.jpg';
                } else if(result[index].gender == 'female') {
                    linkedin_pic = 'assets/images/female.png';
                } else {
                    linkedin_pic = 'assets/images/random-profile.png';
                }
            }
            
            sno+=1;
            if(ctr == 0){
                tr += '<div class="card-deck">';
            }
            tr += '<div class="col-md-4">';
            tr += '<div class="card">';
            tr += '<div class="twenty-spacer"></div>';
            tr += '<div class="text-center"><img class="card-img-top rounded-circle directory_img modal-member member-' + id + '" data-first_name= "'+ first_name +'" data-last_name = "' + last_name + '" data-linkedin_pic = "' + linkedin_pic + '" data-occupation = "' + occupation + '" data-company = "' + company + '" data-industry = "' + industry + '"  data-joined = "' + first_bill_date + '" datadata-member_id = "' + id + '" src="' + linkedin_pic + '" data-bio = "' + bio + '" data-braintree_customer_id = "' + braintree_customer_id + '" data-email = "' + email + '" alt="Card image cap" onerror="imgError(this, '+ gender + ', ' + id + ');"></div>';
            tr += '<div class="card-body text-center">';
            tr += '<h5 class="card-title modal-member member-' + id + '" data-first_name= "'+ first_name +'" data-last_name = "' + last_name + '" data-linkedin_pic = "' + linkedin_pic + '" data-occupation = "' + occupation + '" data-company = "' + company + '" data-industry = "' + industry + '"  data-joined = "' + first_bill_date + '" data-member_id = "' + id + '" data-bio = "' + bio + '" data-braintree_customer_id = "' + braintree_customer_id + '" data-email = "' + email + '">'+ first_name + ' ' + last_name + '</h5>';
            tr += '</div>';
            tr += '</div>';
            tr += '</div>';
            if(ctr == 2){
                tr += '</div>';
                tr += '<div class="twenty-spacer"></div>';
    
                ctr = 0;
            } else {
                ctr++;
            }
            ctr2++;
        }
    } else {
        tr = '<p>There are no members in <strong>' + $('select#member_city option:selected').text() + '</strong>.</p>';
    }
    $('div.memberdirectory_holder').append(tr);
}

$(document).on('click', '.directory_img', function(){
    $('.loadingDiv').show();
    var member_name = $(this).data('first_name') + ' ' + $(this).data('last_name');
    var member_pic = $(this).attr('src');//$(this).data('linkedin_pic');
    var member_occupation = $(this).data('occupation');
    var member_company = $(this).data('company');
    var member_industry = $(this).data('industry');
    var member_joined = $(this).data('joined');
    var member_bio = $(this).data('bio');
    var member_id = $(this).data('member_id');
    var member_braintree_customer_id = $(this).data('braintree_customer_id');
    var member_email = $(this).data('email');
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    var d = new Date(member_joined);
    var member_month = month[d.getMonth()];
    var member_year = d.getFullYear();
    var member_address = '';
    var intro = '';
    if((member_occupation != '' && member_occupation != null) && (member_company != '' && member_company != null)){
        intro = member_occupation + ' at ' + member_company;
    } else {
        if(member_occupation != '' && member_occupation != null){
            intro = member_occupation;
        } else if(member_company != '' && member_company != null){
            intro = member_company;
        } else {
            intro = '';
        }
    }
    if(member_bio == '' || member_bio == null){
        member_bio = $(this).data('first_name') + ' hasn\'t added a Bio.';
    }
    if(member_occupation == '' || member_occupation == null){
        member_occupation = $(this).data('first_name') + ' hasn\'t added an Occupation.';
    }
    if(member_industry == '' || member_industry == null){
        member_industry = $(this).data('first_name') + ' hasn\'t added an Industry.';
    }
    
    $.ajax({
        url: window.base_url + 'member/mdirectory/get_address',
        type: 'get',
        data: {'braintree_id': member_braintree_customer_id},
        dataType: 'json',
        success: function(response){
            $('.loadingDiv').hide();
            /*
            $data = array(
                'street_address' => '',
                'extended_address' => '',
                'locality' => '',
                'postalcode' => '',
                'countrycode' => ''
            );
            */
            if (response.street_address == '' && response.extended_address == '' && response.locality == '' && response.postalcode == '' && response.countrycode == '') {
                member_address = '';
            } else {
                var address = '';
                member_address = (response.street_address != '') ? response.street_address + ', ':'';
                member_address += member_address + (response.extended_address != '') ? response.extended_address + ', ':'';
                member_address += member_address + (response.locality != '') ? response.locality + ', ':'';
                member_address += member_address + (response.postalcode != '') ? response.postalcode + ', ':'';
                member_address += member_address + (response.countrycode != '') ? response.countrycode:'';
                member_address = '<li class="list-group-item text-muted bg-pink member-address"><i class="fas fa-map-pin"></i><span class="tab small">' + member_address + '</span></li>';
            }
            
            var member_bootbox = bootbox.alert({
                className: 'directory-modal',
                message: '<div class="text-center"><img src="' + member_pic + '" class="img-thumbnail rounded-circle" width="120"><h5>' + member_name + '</h5><h6>' + intro + '</h6></div><hr class="hr-line" style="width:50%" /><div class="text-left"><h6><span class="bot-border">Bio</span></h6><p>' + member_bio + '</p><h6><span class="bot-border">Current Occupation</span></h6><p>' + member_occupation + '</p><h6><span class="bot-border">Industry</span></h6><p>' + member_industry + '</p><ul class="list-group list-group-flush text-left member-details">' + member_address + '<li class="list-group-item text-muted bg-pink"><i class="far fa-calendar"></i><span class="tab small">Joined: ' + member_month + ' ' + member_year + '</span></li><li class="list-group-item text-muted bg-pink"><i class="far fa-envelope"></i><span class="tab small">' + member_email + '</span></li><li class="list-group-item text-muted bg-pink text-center"><button class="btn btn-brunchwork contact-member-form" data-to="' + member_email + '">CONTACT</button></li></ul></div>'
            });
        
            member_bootbox.init(function(){
                $(this).find('.modal-body').addClass('bg-pink');
                $(this).find('.modal-footer').hide();
            });
        }
    });
});

$(document).on('click', 'button.contact-member-form', function(){
    $('.loadingDiv').hide();
    email_to = $(this).data('to');
    orig_content = $('.directory-modal').find('.bootbox-body').html();
    $('.directory-modal').find('.bootbox-body').empty();
    $('.directory-modal').find('.bootbox-body').load(window.base_url + 'member/mdirectory/contact_member', { email: email_to });
    $('input#dir_to_email').val(email_to);
});

$(document).on('click', 'button.cancel_dir_send_email', function(){
    //$('.directory-modal').modal('hide');
    $('.directory-modal').find('.bootbox-body').empty();
    $('.directory-modal').find('.bootbox-body').html(orig_content);

});

$(document).on('click', 'button.clear_dir_email', function(){
    $('#contact-memberdir-form')[0].reset();
});

$(document).on('click', 'button.send_dir_email', function(){
    $('.loadingDiv').show();
    if($('input#dir_to_email').val() == null){
        $('label.error').css('display', 'none');
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid red');
    } else {
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid #ced4da;')
    }
    if($("#contact-memberdir-form").valid()){
        var contact_member_data = $("#contact-memberdir-form").serializeArray();
        $.ajax({
            type: "POST", 
            url: window.base_url+'user/contact_member', 
            data: contact_member_data,
            dataType : 'JSON',
            success: function (response) {
                $('.loadingDiv').hide();
                $('#contact-memberdir-form')[0].reset();
                $('.directory-modal').find('.bootbox-body').prepend('<div class="twenty-spacer"></div><div class="alert alert-' + response.alert_type + '" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.message + '</div>');
            },
                error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log("There was an error: " + errorThrown);  
            }
        });
    } else {
        $('label.error').css('display', 'none');
    }
});