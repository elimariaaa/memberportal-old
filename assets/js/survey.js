function skipSurvey(){
    bootbox.confirm({
        title: "Skip Survey",
        message: '<p class="text-left">Are you sure you want to skip the survey?</p>',
        callback: function (result) {
            console.log(result);
            if(result == null || result == false) {
                $(this).modal('hide');
            } else {
                window.location.href = window.base_url + 'member/confirm_cancel';
            }
        }
    });
}

$(document).on('click', 'button.skip-survey', function(){
    skipSurvey();
});

$(document).on('click', 'button.submit-survey', function(){
    $.ajax({
        url: window.base_url + 'member/submit_survey',
        type: "POST",
        dataType:'JSON',
        data: $('form#cancel_survey').serializeArray(),
        success: function(response) {
            console.log(response);
            if(response.form == 'empty'){
                skipSurvey();
            } else {
                if(response.success == true){
                    window.location.href = window.base_url + 'member/confirm_cancel';
                }
            }
        },
        error: function (MLHttpRequest, textStatus, errorThrown) {
          console.log("There was an error: " + errorThrown);
        }
    });
});