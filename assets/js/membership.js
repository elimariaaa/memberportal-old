var compare_date = '';
var cancel_message = '';
var cancel_dialog = '';
$(document).on('click', 'button.pause-membership', function(){
    bootbox.confirm({
        title: "Pause Membership",
        message: '<p class="text-left">Are you sure you want to pause your membership?</p>',
        callback: function (result) {
            console.log(result);
            if(result == null || result == false) {
                $(this).modal('hide');
            } else {
                if(result == true){
                    $.ajax({
                        url: window.base_url + 'member/pause_subscription',
                        type: "GET",
                        dataType:'JSON',
                        success: function(response) {
                            console.log(response);
                            if(response.success == true){
                                window.location.href = window.base_url + 'membership';
                            }
                            
                        },
                        error: function (MLHttpRequest, textStatus, errorThrown) {
                          console.log("There was an error: " + errorThrown);
                        }
                    });
                }
            }
        }
    });
});

$(document).on('click', 'button.unpause-membership', function(){
    bootbox.confirm({
        title: "Continue with Membership",
        message: '<p class="text-left">Are you sure to continue with your membership?</p>',
        buttons: {
            confirm: {
                label: 'Yes'
            },
            cancel: {
                label: 'No'
            }
        },
        callback: function (result) {
            console.log(result);
            if(result == null || result == false) {
                $(this).modal('hide');
            } else {
                if(result == true){
                    $.ajax({
                        url: window.base_url + 'member/unpause_subscription',
                        type: "GET",
                        dataType:'JSON',
                        success: function(response) {
                            console.log(response);
                            if(response.success == true){
                                window.location.href = window.base_url + 'membership';
                            }
                            
                        },
                        error: function (MLHttpRequest, textStatus, errorThrown) {
                          console.log("There was an error: " + errorThrown);
                        }
                    });
                }
            }
        }
    });
});

function dateValidation() {
    var b = new Date(first_bill_date); //your input date here

    var d = new Date(); // today date
    d.setMonth(d.getMonth() - 6);  //subtract 6 months from current date 

    if (b > d) {
        //alert("date is less than 6 month");
        compare_date = 'less';
    } else {
        //alert("date is older than 6 month");
        compare_date = 'more';
    }
    return compare_date;
}

$(document).on('click', 'button.cancel-membership', function(){
    var check_date = dateValidation();
    if(check_date == 'less'){
        cancel_message = '<p>Membership is a 6-month commitment. You can cancel at the end of 6 months.</p>';
    } else if(check_date == 'more'){
        cancel_message = '<p>We are confirming your membership cancellation.</p>';
        cancel_message += '<p>We would love your feedback on your membership experience. We shall use your feedback to improve the experience for current & future members.</p>';
        cancel_message += '<p>This <a class="btn-link" href="#" id="quick_survey">quick survey</a> shall take you 3 minutes.</p>';
    }

    cancel_dialog = bootbox.confirm({
        title: "Cancel Membership",
        message: cancel_message,
        buttons: {
            confirm: {
                label: 'Cancel My Membership',
                className: 'btn-brunchwork cancel_mem' //cancel_membership_ok
            },
            cancel: {
                label: 'Proceed With Survey',
                className: 'btn-brunchwork proc_sur'
            }
        },
        callback: function (result) {
            console.log(result);
            if(result == null) {
                $(this).modal('hide');
            } else {
                if(result == true){
                    $.ajax({
                        url: window.base_url + 'member/cancel_subscription',
                        type: "GET",
                        dataType:'JSON',
                        success: function(response) {
                            console.log(response);
                            if(response.success == true){
                                cancel_dialog.modal('hide');
                                window.location.href = window.base_url + 'member/confirm_cancel';
                            }
                            
                        },
                        error: function (MLHttpRequest, textStatus, errorThrown) {
                          console.log("There was an error: " + errorThrown);
                        }
                    });
                } else if(result == false){
                    cancel_subscription();
                }
                return false;
            }
        }
    });

    cancel_dialog.init(function(){
        $('.cancel_membership_ok').css('display', 'none');
    });
});

$(document).on('click', 'a#quick_survey', function(){
    cancel_subscription();
});

function cancel_subscription(){
    $.ajax({
        url: window.base_url + 'member/cancel_subscription',
        type: "GET",
        dataType:'JSON',
        success: function(response) {
            console.log(response);
            if(response.success == true){
                cancel_dialog.modal('hide');
                //window.location.href = 'https://goo.gl/forms/xgSrAx7cTuyZ9NDH3';
                window.location.href = window.base_url + 'member/cancel';
            }
            
        },
        error: function (MLHttpRequest, textStatus, errorThrown) {
          console.log("There was an error: " + errorThrown);
        }
    });
}