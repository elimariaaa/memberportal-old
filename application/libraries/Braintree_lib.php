<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH.'third_party/Braintree/Braintree.php';

/*
 *  Braintree_lib
 *	Braintree PHP SDK v3.*
 *  For Codeigniter 3.*
 */

class Braintree_lib{

		function __construct() {
			$CI = &get_instance();
			$this->CI = &get_instance();
			$CI->config->load('braintree', TRUE);
			$braintree = $CI->config->item('braintree');
			$CI->load->library('ion_auth');
			/*
			if($braintree['braintree_environment'] == 'sandbox'){
				Braintree_Configuration::environment($braintree['braintree_environment']);
				Braintree_Configuration::merchantId('tdvy5qhfcy5nr5zr');
				Braintree_Configuration::publicKey('y5xvfn4rh65wyvqx');
				Braintree_Configuration::privateKey('d4f183162e4c0882f25274b4f13f2b80');
			}
			if($braintree['braintree_environment'] == 'production'){
				Braintree_Configuration::environment($braintree['braintree_environment']);
				Braintree_Configuration::merchantId($braintree['braintree_merchant_id']);
				Braintree_Configuration::publicKey($braintree['braintree_public_key']);
				Braintree_Configuration::privateKey($braintree['braintree_private_key']);
			}
			*/
			//sandbox
			$this->environment = $braintree['braintree_environment'];
			if($braintree['braintree_environment']  == 'sandbox'){
				$this->gateway = new Braintree_Gateway([
					'environment' => $braintree['braintree_environment'],
					'merchantId' => 'tdvy5qhfcy5nr5zr',
					'publicKey' => 'y5xvfn4rh65wyvqx',
					'privateKey' => 'd4f183162e4c0882f25274b4f13f2b80'
				]);
			} else if($braintree['braintree_environment']  == 'production'){
				$this->gateway = new Braintree_Gateway([
					'environment' => $braintree['braintree_environment'],
					'merchantId' => $braintree['braintree_merchant_id'],
					'publicKey' => $braintree['braintree_public_key'],
					'privateKey' => $braintree['braintree_private_key']
				]);
			}
		}

    function create_client_token(){
		$user = $this->CI->ion_auth->user()->row();
    	$clientToken = $this->gateway->clientToken()->generate([
			"customerId" => $user->braintree_customer_id
		]);
    	return $clientToken;
	}
	
	function get_all_clients(){
		return $this->gateway->customer()->all();
	}

	function client_fetch_id($ids)
    {
        return $this->gateway->customer()->search([
			Braintree_CustomerSearch::id()->is($customer_id)
		]);
	}
	
	function active_subscriptions(){
		$subscriptions = $this->gateway->subscription()->search([
			Braintree_SubscriptionSearch::status()->in([Braintree_Subscription::ACTIVE])
		  ]);
		$data = array();
		$counter = 30;
		foreach($subscriptions AS $member_data){
			if(!$counter != 0){
				$data[] = $member_data->ids;
			}
			$counter--;
		}
		return $data;
	}

	function all_plans(){
		
		return $this->gateway->plan()->all();
	}

	function find_client($parameter, $value){
		if($parameter == 'email'){
			return $this->gateway->customer()->search([
				Braintree_CustomerSearch::email()->is($value)
			]);
		}
	}

	function payment_method($token){
		return $this->gateway->paymentMethod()->find($token);
	}

	function get_transactions($customer_id){
		return $this->gateway->transaction()->search([
			Braintree_TransactionSearch::customerId()->is($customer_id),
		  ]);
	}

	function update_cc($token, $attributes){
		return $this->gateway->creditCard()->update($token, $attributes);
	}

	function create_cc($customer_id, $attributes){
		$nonce = '';
		//$this->create_client_token()
		if($this->environment == 'production'){
			$result = $this->gateway->paymentMethodNonce()->create($attributes['token']);//$this->gateway->paymentMethodNonce()->create($this->create_client_token());
			$nonce = $result->paymentMethodNonce->nonce;
			/*
			$paymentMethodNonce = $this->gateway->paymentMethodNonce()->find($nonce);
			$info = $paymentMethodNonce->threeDSecureInfo;
			if (empty($info)) {
				// This means that the nonce was not 3D Secured
				return;
			}
			$info->enrolled;
			$info->status;
			$info->liabilityShifted;
			$info->liabilityShiftPossible;
			*/
		} else if($this->environment == 'sandbox'){
			$nonce = 'fake-valid-nonce';
		}
		if($attributes['token']){
			return $result = $this->gateway->creditCard()->update($attributes['token'],[
				//'customerId' => $customer_id,
				//'paymentMethodNonce' => $nonce,
				'cvv' => $attributes['cvv'],
				'number' => $attributes['number'],
				'expirationMonth' => $attributes['expirationMonth'],
				'expirationYear' => $attributes['expirationYear'],
				'options' => [
					'makeDefault' => true
					//'failOnDuplicatePaymentMethod' => true,
					//'verifyCard' => true
				  ]
				]);
		} else {
			return $result = $this->gateway->creditCard()->create($attributes['token'],[
				'customerId' => $customer_id,
				'cvv' => $attributes['cvv'],
				'number' => $attributes['number'],
				'expirationMonth' => $attributes['expirationMonth'],
				'expirationYear' => $attributes['expirationYear'],
				'options' => [
					'makeDefault' => true
					//'failOnDuplicatePaymentMethod' => true,
					//'verifyCard' => true
				  ]
				]);
		}
			/*
		return $this->gateway->paymentMethod()->create([
		'customerId' => $customer_id,
		'paymentMethodNonce' => $nonce,
		//'cvv' => $attributes['cvv'],
		'number' => $attributes['number'],
		'expirationMonth' => $attributes['expirationMonth'],
		'expirationYear' => $attributes['expirationYear'],
		'options' => [
			'makeDefault' => true,
			'failOnDuplicatePaymentMethod' => true,
			//'verifyCard' => true
		  ]
		]);
		*/
	}

	function customer_address($customer_arr){
		//find if there's address existing
		$data = array();
		$data['success'] = false;
		$user = $this->CI->ion_auth->user()->row();
		if($customer_arr['address_id']){
			$result =  $this->gateway->address()->update($user->braintree_customer_id, $customer_arr['address_id'], [
				'streetAddress'     	=> $customer_arr['street'],
				'extendedAddress'   	=> $customer_arr['extended'],
				'locality'          	=> $customer_arr['city'],
				'region'            	=> $customer_arr['state'],
				'postalCode'        	=> $customer_arr['postal'],
				'countryCodeAlpha2' 	=> $customer_arr['countryCodeAlpha2'],
				'countryCodeAlpha3' 	=> $customer_arr['countryCodeAlpha3'],
				'countryCodeNumeric'	=> $customer_arr['countryCodeNumeric'],
				'countryName'			=> $customer_arr['countryName']
			  ]);
			$data['action'] = 'update';
			if($result){
				$data['success'] = true;
			}
		} else {
			$result = $this->gateway->address()->create([
				'customerId'			=> $user->braintree_customer_id,
				'streetAddress'     	=> $customer_arr['street'],
				'extendedAddress'   	=> $customer_arr['extended'],
				'locality'          	=> $customer_arr['city'],
				'region'            	=> $customer_arr['state'],
				'postalCode'        	=> $customer_arr['postal'],
				'countryCodeAlpha2' 	=> $customer_arr['countryCodeAlpha2'],
				'countryCodeAlpha3' 	=> $customer_arr['countryCodeAlpha3'],
				'countryCodeNumeric'	=> $customer_arr['countryCodeNumeric'],
				'countryName'			=> $customer_arr['countryName']
			]);
			$data['action'] = 'save';
			if($result){
				$data['success'] = true;
			}
		}
		return $data;
	}

	function get_all_plans(){
		return $plans = $this->gateway->plan()->all();
	}

	function cancel_subscription(){
		$data = array();
		$data['subscription_status'] = '';
		$data['success'] = false;
		$user = $this->CI->ion_auth->user()->row();
		$result = $this->gateway->subscription()->cancel($user->braintree_subscription_id);
		if($result){
			//check if subscription status is canceled to be sure
			$subscription = $this->gateway->subscription()->find($user->braintree_subscription_id);
			$data['subscription_status'] = $subscription->status;
			if(strtolower($subscription->status) == 'canceled'){
				$data['success'] = true;
			}
		}
		return $data;
	}

	function pause_subscription(){
		$data = array();
		$data['subscription_status'] = '';
		$data['is_paused'] = '';
		$data['success'] = false;
		$user = $this->CI->ion_auth->user()->row();
		$subscription = $this->gateway->subscription()->find($user->braintree_subscription_id);
		$subscription_price = $subscription->price;
		$subscription_plan = $subscription->planId;
		$subscription_status = $subscription->status;
		$discount = $subscription_plan.'-discount';
		$result = $this->gateway->subscription()->update($user->braintree_subscription_id, [
			'discounts' => [
				'add' => [
					[
						'inheritedFromId' => $discount,
						'amount' => $subscription_price
					]
				]
			]
		]);
		if($result){
			$data['success'] = true;
			$data['is_paused'] = 1;
			$data['subscription_status'] = $subscription_status;
		}
		return $data;
	}

	function unpause_subscription(){
		$data = array();
		$data['subscription_status'] = '';
		$data['success'] = false;
		$data['is_paused'] = '';
		$user = $this->CI->ion_auth->user()->row();
		$subscription = $this->gateway->subscription()->find($user->braintree_subscription_id);
		$subscription_price = $subscription->price;
		$subscription_plan = $subscription->planId;
		$discount = $subscription_plan.'-discount';
		$result = $this->gateway->subscription()->update($user->braintree_subscription_id, [
			'discounts' => [
				'remove' => [$discount]
			]
		]);
		if($result){
			$data['success'] = true;
			$subscription = $this->gateway->subscription()->find($user->braintree_subscription_id);
			$subscription_status = $subscription->status;
			$data['subscription_status'] = $subscription_status;
			$data['is_paused'] = 0;
		}
		return $data;
	}

	function member_address($braintree_id){
		//get address id
		return $customer = $this->gateway->customer()->find($braintree_id);
	}
}
