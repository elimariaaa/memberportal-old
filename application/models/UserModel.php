<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ion Auth Model
 * @property Bcrypt $bcrypt The Bcrypt library
 * @property Ion_auth $ion_auth The Ion_auth library
 */
class UserModel extends CI_Model
{

    function get_cities(){
        return $this->db->where('active', 1)
                        ->get('ci_city_membership')->result_array();
    }

    function get_membership_plan(){
        $user = $this->ion_auth->user()->row();
        return $this->db->where('id', $user->ci_membership_plan_id)
                        ->get('ci_membership_plan')->row();
    }

    function get_city(){
        $user = $this->ion_auth->user()->row();
        return $this->db->where('id', $user->city_id)
                        ->get('ci_city_membership')->row();
    }

    function get_states(){
        $states = $this->db->get('ci_states')->result_array();
        $states_data = '<option value="" selected disabled>Select one.</option>';
        foreach($states AS $states_db_data){
            $states_data .= '<option value="'.$states_db_data['state_code'].'">'.$states_db_data['state_name'].'</option>';
        }
        return $states_data;
    }

    function get_industry(){
        return $this->db->select('industry')->where('industry !=', '')->group_by('industry')->order_by('industry', 'ASC')->get('ci_users')->result_array();
    }

    function submit_credit_card($cardnumber = false, $expiry_month = false, $expiry_year = false, $cvc = false){
        $user = $this->ion_auth->user()->row();
        $status = array();
        if($cardnumber && $expiry_month && $expiry_year && $cvc){
            if($this->check_card()){
                $update = $this->db->set('card_no', $cardnumber)
                                ->set('expiry_month', $expiry_month)
                                ->set('expiry_year', $expiry_year)
                                ->set('cvc', $cvc)
                                ->where('user_id', $user->id)
                                ->update('ci_credit_card');
                if($update){
                    return $status['status'] = 'update';
                } else {
                    return $status['status'] = 'false'; 
                }
            } else {
                $data = array(
                    'user_id' => $user->id,
                    'card_no' => $cardnumber,
                    'expiry_month' => $expiry_month,
                    'expiry_year' => $expiry_year,
                    'cvc' => $cvc
                );
            
                $insert = $this->db->insert('ci_credit_card', $data);
                if($insert){
                    return $status['status'] = 'insert';
                } else {
                    return $status['status'] = 'false'; 
                }
            }
        } else {
            return false;
        }
    }

    function get_countries(){
        return $this->db->get('ci_countries')->result_array();
    }

    function update_email(){
        $user = $this->ion_auth->user()->row();
        $data = array();
        $data['success'] = false;
        $data['message'] = '';
        //lindsey@aerointeractive.com
        //check first if email already exists
        $check_email_yours = $this->db->where('email', html_escape($this->input->post('update_email')))->where('id', $user->id)->get('ci_users')->num_rows();
        $check_email_not_yours = $this->db->where('email', html_escape($this->input->post('update_email')))->where('id !=', $user->id)->get('ci_users')->num_rows();
        if($check_email_yours > 0){
            $data['message'] = 'You entered the same email. Please try again.';
            return $data;
        }

        if($check_email_not_yours > 0){
            $data['message'] = 'The email you entered is not yours. Please try again.';
            return $data;
        }
        if(!$check_email_not_yours && !$check_email_yours){
            $this->db->set('email', html_escape($this->input->post('update_email')));
            $this->db->set('username', html_escape($this->input->post('update_email')));
            $this->db->where('id', $user->id);
            $update_email = $this->db->update('ci_users');
            $data['success'] = $update_email;
            return $data;
        }
    }

    function update_profile(){
        $user = $this->ion_auth->user()->row();
        $this->db->where('id', $user->id);
        return $this->db->update('ci_users', $this->input->post());
    }

    function get_members(){
        if($this->input->get()){
            $this->db->where('email LIKE "%'.$this->input->get('q').'%" OR first_name LIKE "%'.$this->input->get('q').'%" OR last_name LIKE "%'.$this->input->get('q').'%" OR CONCAT(first_name, " ", last_name) LIKE "%'.$this->input->get('q').'%"');
        }
        return $member_data = $this->db->select('CONCAT(first_name, \' \', last_name) AS text')->select('email as id')->where('active', 1)->order_by('last_name', 'ASC')->get('ci_users')->result_array();
    }

    function update_user($update_data_arr){
        $user = $this->ion_auth->user()->row();
        return $this->db->where('id', $user->id)->update('ci_users', $update_data_arr);
    }

    function insert_survey(){
        $this->db->insert('ci_cancel_membership_survey', $this->input->post());
        $last_inserted_id = $this->db->insert_id();
        $user = $this->ion_auth->user()->row();
        $id_arr = array('user_id' => $user->id);
        return  $this->db->where('id', $last_inserted_id)->update('ci_cancel_membership_survey', $id_arr);
    }

}