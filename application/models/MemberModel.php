<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ion Auth Model
 * @property Bcrypt $bcrypt The Bcrypt library
 * @property Ion_auth $ion_auth The Ion_auth library
 */

class MemberModel extends CI_Model
{
    public function __construct() {
        parent::__construct(); 
    }

    function get_members($rowno,$rowperpage){
        
        $user = $this->ion_auth->user()->row();
        if($this->input->get()){
            if($this->input->get('member_data')){
                $member = $this->input->get('member_data');
                $where = "(CONCAT(`first_name`, ' ', `last_name`) LIKE '%".$member."%' OR first_name LIKE '%".$member."%' OR last_name LIKE '%".$member."%')";
                $this->db->where($where);
            }
            if($this->input->get('member_city')){
                $member_city = $this->input->get('member_city');
                $this->db->where('city_id', $member_city);
            }
        }
        $this->db->where('id !=', $user->id);
        $this->db->where('active', 1);
        $this->db->limit($rowperpage, $rowno);
        $this->db->order_by('last_name', 'asc');
        return $this->db->get('ci_users')->result_array();
    }

    // Select total records
    public function getrecordCount() {
        if($this->input->get()){
            if($this->input->get('member_data')){
                $member = $this->input->get('member_data');
                $where = "(CONCAT(`first_name`, ' ', `last_name`) LIKE '%".$member."%' OR first_name LIKE '%".$member."%' OR last_name LIKE '%".$member."%')";
                $this->db->where($where);
            }
            if($this->input->get('member_city')){
                $member_city = $this->input->get('member_city');
                $this->db->where('city_id', $member_city);
            }
        }
        $this->db->select('count(*) as allcount');
        $this->db->where('active', 1);
        $this->db->from('ci_users');
        $query = $this->db->get();
        $result = $query->result_array();

        return $result[0]['allcount'];
    }

    function get_all_city(){
        return $this->db->order_by('id')->get('ci_city_membership')->result_array();
    }
}