<div class="box shadowed-box">
    <form class="form-signin text-center" method="POST">
        <img src="<?php echo base_url('assets/images/brunchwork-logo.png'); ?>" alt="">
        <div class="fifty-spacer"></div>
        <h1 class="h3 mb-3 font-weight-normal">Login</h1>
        <?php
            if(isset($message) && $message != ''){
        ?>
        <div class="alert alert-success text-left" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $message; ?>
        </div>
        <?php            
            }
        ?>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name="username" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <div class="checkbox mb-3">
        <label>
            <input type="checkbox" name="remember_me" value="remember-me"> Remember me
        </label>
        </div>
        <a href="<?php echo base_url(); ?>forgot_password">Forgot your password?</a>
        <?php
                $csrf = array(
                    'name' => $this->security->get_csrf_token_name(),
                    'hash' => $this->security->get_csrf_hash()
                    );
        ?>
        <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
        <button class="btn btn-lg btn-brunchwork btn-block" type="submit">Sign in</button>
        <div class="twenty-spacer"></div>
        Sign Up or Apply to be a member
    </form>
</div>