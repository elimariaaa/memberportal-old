<?php
    $user = $this->ion_auth->user()->row();
    //print_r($user);
?>
<div class="twenty-spacer"></div>
<h1>Event Calendar</h1>
<div class="ten-spacer"></div>
<p>Showing all <strong>Upcoming Events</strong> of brunchwork.</p>
<div class="twenty-spacer"></div>
<?php
    if($message){
?>
<div class="events_alert mb-2">
    <div class="alert alert-danger text-left city_alert" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
</div>
<?php
    }
?>
<h4>Advanced Search</h4>
<div class="card-deck">
	<div class="card col-md-12 bg-pink no-border">
		<div class="card-body">
            <?php
                $token = "5JVTZDONWCOPI2WUIWST";
                $organizer_id =  "7806405987";
                //$request_url = "https://www.eventbriteapi.com/v3/events/search/?organizer.id=".$organizer_id."&token=".$token;
                $request_url = "https://www.eventbriteapi.com/v3/events/search/?sort_by=date&organizer.id=".$organizer_id."&token=".$token;
                $params = array('sort_by' => 'date', 'organizer.id' => $organizer_id, 'token' => $token);
                $context = stream_context_create(
                                array(
                                    'http' => array(
                                        'method'  => 'GET',
                                        'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                                        //'content' => http_build_query($params)
                                    )
                                )
                            );
                $json_data = file_get_contents( $request_url, false, $context );
                //$response = get_object_vars(json_decode($json_data));
                $response = json_decode($json_data, true);
                $get_count = $response['pagination']['object_count'];
                for($x = 0; $x < $get_count; $x++){
                    $event_date = date('Y-M-d', strtotime($response['events'][$x]['start']['local']));
                    $start_event_time = date('g:i A', strtotime($response['events'][$x]['start']['local']));
                    $end_event_time = date('g:i A', strtotime($response['events'][$x]['end']['local']));
                    $day = date('l', strtotime($response['events'][$x]['start']['local']));
                    $calendar = explode('-', $event_date);
                    $whole_date = date('F j, Y', strtotime($response['events'][$x]['start']['local']));
                    $calendar_date = $calendar[2];
                    $calendar_month = $calendar[1];

            ?>
            <div class="calendar-container container">
                <article id="event-<?php echo $response['events'][$x]['id']; ?>" class="d-flex justify-content-center">	
                    <div class="calendar col-md-12 col-sm-12 col-xs-12">
                        <div class="date active col-md-1 col-sm-12 col-xs-12">
                            <span class="calendar-date"><?php echo $calendar_date; ?></span>
                            <p class="calendar-month"><?php echo $calendar_month; ?></p>
                        </div>
                        <div class="cal-img col-md-7 col-sm-12 col-xs-12">
                        <?php 
                                    // if(function_is_formated_event(get_post()->post_content))
                                    // 	$eventurltoecho=esc_url( eventbrite_venue_get_wp_event_url( get_post() )) ;
                                    // else
                                    

                            ?>
                            <!-- <img src="http://lorempixel.com/717/717/nature/" class="img-responsive">-->
                        <a style="cursor: default;" class="post-thumbnail" href="javascript:void(0);" class="wp-post-image"  >
                        <img src="<?php echo $response['events'][$x]['logo']['original']['url']; ?>" class="img-fluid" />
                        </a>
                        </div>
                        <div class="cal-desc col-md-4 col-sm-12 col-xs-12"> 
                            <h4 class="calendar-date"><a href="#" rel="bookmark"><?php echo $response['events'][$x]['name']['text']; ?></a></h4>		  
                            <div class="col text-center rsvp-button">
                                <h6 class="calendar-month"><?php echo $day.', '.$whole_date; ?></h6>
                                <h6 class="calendar-month"><?php echo $start_event_time.' to '.$end_event_time; ?></h6>
                                <?php
                                    if($user->eventbrite_token){
                                        $eventbrite_url = $response['events'][$x]['url'];
                                    } else {
                                        $eventbrite_url = "https://www.eventbrite.com/oauth/authorize?response_type=code&client_id=".EVENTBRITE_APP_KEY;
                                    }
                                ?>
                                <a href="<?php echo $eventbrite_url; ?>" class="btn btn-lg btn-brunchwork">RSVP</a>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <?php
                }
            ?>
        </div>
    </div>
    <!--
    <div class="card col-md-4">
		<div class="card-body">
        </div>
    </div>
    -->
</div>