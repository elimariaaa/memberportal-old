<div class="box shadowed-box">
<?php $attributes = array('class' => 'form-signin text-center'); ?>
<?php echo form_open(base_url('forgot_password'), $attributes);?>
<img src="<?php echo base_url('assets/images/brunchwork-logo.png'); ?>" alt="">
<div class="fifty-spacer"></div>
<h1 class="h3 mb-3 font-weight-normal">Forgot your password?</h1>
<?php
      if(isset($message) && $message != ''){
?>
<div class="alert alert-danger text-left" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
      </button>
      <?php echo $message; ?>
</div>
<?php            
      }
?>
<p class="text-center normal-text">Enter your email below and we'll send you a link to reset your password.</p>
<label for="inputEmail" class="sr-only">Email address</label>
<input type="email" name="username" id="inputEmail" class="form-control forgotpass" placeholder="Enter Your Email" required autofocus>
<div class="twenty-spacer"></div>
<button class="btn btn-lg btn-brunchwork btn-block btn-signin" type="submit">SUBMIT</button>
<p class="mt-5 mb-3 text-muted"><!--&copy; 2018--></p>
<?php echo form_close();?>
</div>