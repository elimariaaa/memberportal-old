<?php $user = $this->ion_auth->user()->row(); ?>
<div id="contact-member-form-content">
    <div class="container">
        <div class="ten-spacer"></div>
        <div class="contact_member_alert"></div>
        <form id="contact-memberdir-form" method="post">
            <div class="form-group">
                <label for="from_email">From</label>
                <input type="email" class="form-control" id="from_email" name="from_email" placeholder="From Email" value="<?php echo $user->email; ?>" required>
            </div>
            <div class="form-group">
                <label for="to_email">To</label>
                <input type="text" id="dir_to_email" name="to_email" class="form-control" value="<?php echo $to; ?>" required readonly />
            </div>
            <div class="form-group">
                <label for="subject_email">Subject</label>
                <input type="text" class="form-control" id="subject_email" name="subject_email" placeholder="Email Subject" required>
            </div>
            <div class="form-group">
                <label for="body_email">Message</label>
                <textarea class="form-control" rows="5" id="body_email" name="body_email" required></textarea>
            </div>
            <div class="form-group">
                <?php
                    $csrf = array(
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    );
                ?>
                <input type="hidden" id="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <button type="button" class="btn btn-lg send_dir_email btn-link" style="padding: 5px;">Send</button> | 
                        <button type="button" class="btn btn-lg clear_dir_email btn-link" style="padding: 5px;">Clear</button>
                    </div>
                    <div class="col-md-4 text-right">
                        <div class="float-right"><button type="button" class="btn btn-lg cancel_dir_send_email btn-link" style="padding: 5px;">Cancel</button></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="center-parent loadingDiv"><div class="center-container"></div></div>
<script>
    $(document).ready(function(){
        $('.loadingDiv').hide();
    });
</script>