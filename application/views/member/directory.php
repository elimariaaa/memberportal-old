<?php
    $user = $this->ion_auth->user()->row();
    //print_r($user);
?>

<div class="twenty-spacer"></div>
<div class="container">
    <h1>Member Directory</h1>
    <div class="ten-spacer"></div>
    <p>Showing all <strong>Active</strong> members of brunchwork.</p>
    <div class="twenty-spacer"></div>
    <h4>Advanced Search</h4>
    <div class="padding-topbottom container">
        <div class="row">
        <div class="col-md-5">
            <div class="input-group input-group-lg">
                <input class="form-control no-radius py-2 memberdir-val" type="search" placeholder="Search by Name" id="example-search-input">
                <span class="input-group-append">
                    <button class="btn btn-outline-secondary search-memberdir" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
        <div class="col-md-5">
            <div class="input-group input-group-lg">
                <!--
                <select class="form-control form-control-lg no-radius" id="member_plan">
                    <option value="" selected disabled>Select Plan.</option>
                    <?php
                    foreach($get_all_plans  AS $plans):
                        if(strpos($plans->id, 'sf-') !== false){
                            $plan_name = 'SF - '.$plans->name;
                        } else {
                            $plan_name = 'NYC - '.$plans->name;
                        }
                        echo "<option value=".$plans->id.">".$plan_name."</option>";
                    endforeach;
                    ?>
                </select>
            -->
                <select class="form-control form-control-lg no-radius" id="member_city">
                    <option value="" selected disabled>Filter by City</option>
                    <?php
                    foreach($cities AS $show_city):
                        echo "<option value=".$show_city['id'].">".$show_city['city']."</option>";
                    endforeach;
                    ?>
                </select>
                <span class="input-group-append">
                    <button class="btn btn-outline-secondary search-membercity" type="button">
                        <i class="fa fa-filter"></i>
                    </button>
                </span>
            </div>
        </div>
        <div class="col-md-2">
			<button type="button" class="btn-lg btn-brunchwork btn memberdir_refresh"><i class="fa fa-redo"></i></button>
        </div>
        </div>
    </div>
    <div class="memberdirectory_holder"></div>
    
   <!-- Paginate -->
   <div style='margin-top: 10px;' id='pagination'></div>

</div>
<div class="hundred-spacer"></div>
<div class="center-parent loadingDiv"><div class="center-container"></div></div>
<script src="<?php echo base_url('assets/js/bootbox.min.js?v=').VER_NO; ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/directory.js?V=').VER_NO; ?>"></script>