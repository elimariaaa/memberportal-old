<div class="twenty-spacer"></div>
<h1>Terms & Conditions</h1>
<div class="card-deck">
	<div class="card col-md-8">
		<div class="card-body">
			<p class="card-text"><small class="text-muted">Last Revised: November 1, 2015</small></p>
			<p>Welcome to www.brunchwork.com. <strong>PLEASE SCROLL DOWN AND READ THIS AGREEMENT IN ITS ENTIRETY BEFORE YOU ACCESS OUR WEBSITE OR USE ANY OF OUR PRODUCTS OR SERVICES.</strong></p>

			<strong>Acceptance of the Terms of Use</strong>

			<p>These terms of use are entered into by and between You, as visitor to this website, and brunchwork (“Company”, “<strong>brunchwork</strong>”, “we” or “us”). The following terms and conditions, together with any documents they expressly incorporate by reference, (collectively, these “Terms of Use”), govern your access to and use of www.brunchwork.com including any content, functionality, and services (collectively the “Service”), or events (“Events”), offered on or through www.brunchwork.com (the “Website”), whether as a guest or a registered user.</p>

			<p><strong>By using the Website you accept and agree to be bound and abide by these Terms of Use, and our Privacy Policy, found <a href="<?php echo base_url('privacy_policy'); ?>" target="_blank">here</a>, incorporated herein by reference.</strong> If you do not want to agree to these Terms of Use or to the Privacy Policy, you must not access or use the Website.</p>

			<p>You may use the Service only if you can form a binding contract with the Company, and only in compliance with these Terms of Use and all applicable local, state, national, and international laws, rules, and regulations. Any use or access to the Website or Service by anyone under 13 is strictly prohibited and in violation of these Terms. By using this Website, you represent and warrant that you are of legal age to form a binding contract with the Company and meet all of the foregoing eligibility requirements. If you do not meet all of these requirements, you must not access or use the Website.</p>

			<p>You may not use the Service if you are a resident of a country embargoed by the United States, or are a foreign person or entity blocked or denied by the United States government.</p>

			<p>All rights not otherwise expressly granted by these Terms of Use are reserved by the Company.</p>

			<strong>Changes to the Terms of Use</strong>

			<p>We may revise and update these Terms of Use from time to time in our sole discretion. When we do, we will revise the “last modified” date given above. All changes are effective immediately when we post them, and apply to all access to and use of the Website thereafter.</p>

			<p>Your continued use of the Website or Services following the posting of revised Terms of Use means that you accept and agree to the changes. It is your responsibility to check this page frequently so you are aware of any changes, as they are binding on you. If you do not agree to any of these terms or any future Terms of Use, do not use or access (or continue to access) the Website or Service.</p>

			<strong>Accessing the Website and Account Security</strong>

			<p>We reserve the right to withdraw or amend this Website, and any Service or material we provide on the Website, in our sole discretion without notice. We will not be liable if for any reason all or any part of the Website is unavailable at any time or for any period. From time to time, we may restrict access to some parts of the Website, or the entire Website, to users, including registered users.</p>

			<p>You are responsible for:</p>

			<ul>
				<li>Making all arrangements necessary for you to have access to the Website.</li>
				<li>Ensuring that all persons who access the Website through your Internet connection are aware of these Terms of Use and comply with them.</li>
			</ul>
			
			<p>To access the Website, some of the resources it offers, or our Events, you may be asked to provide certain registration details or other information. It is a condition of your use of the Website that all the information you provide on the Website is correct, current, and complete. You agree that all information you provide to register with this Website, for our Events or otherwise, including but not limited to, through the use of any interactive features on the Website, is governed by our <a href="<?php echo base_url('privacy_policy'); ?>" target="_blank">Privacy Policy</a>, and you consent to all actions we take with respect to your information consistent with our Privacy Policy.</p>

			<strong>Intellectual Property Rights</strong>

			<p>The Website and its entire contents, features and functionality (including but not limited to all information, software, text, displays, images, video and audio, and the design, selection and arrangement thereof), are owned by the Company, its licensors or other providers of such material and are protected by United States and international copyright, trademark, patent, trade secret, and other intellectual property or proprietary rights laws.</p>

			<p>These Terms of Use grant you a limited, personal, non-exclusive, non-transferable, freely revocable license to use the Website for your personal, non-commercial use only. The Company may terminate this license at any time for any reason or no reason. You must not reproduce, distribute, modify, create derivative works of, publicly display, publicly perform, republish, download, store or transmit any of the material on our Website, except as follows:</p>

			<ul>
			<li>Your computer may temporarily store copies of such materials in RAM incidental to your accessing and viewing those materials.</li>
			<li>You may store files that are automatically cached by your Web browser for display enhancement purposes.</li>
			<li>You may print one copy of a reasonable number of pages of the Website for your own personal, non-commercial use and not for further reproduction, publication or distribution.</li>
			<li>If we provide social media features with certain content, you may take such actions as are enabled by such features.</li>
			</ul>
			
			<p>You must not:</p>

			<ul>
				<li>Modify copies of any materials from this site.</li>
				<li>Use any illustrations, photographs, video or audio sequences or any graphics separately from the accompanying text.</li>
				<li>Delete or alter any copyright, trademark or other proprietary rights notices from copies of materials from this site.</li>
			</ul>
			
			<p>You must not access or use for any commercial purposes any part of the Website or any Services or materials available through the Website.</p>

			<p>If you wish to make any use of material on the Website other than that set out in this section, please address your request to: <a href="mailto:founders@brunchwork.com">founders@brunchwork.com</a></p>

			<p>If you print, copy, modify, download, or otherwise use or provide any other person with access to any part of the Website in breach of the Terms of Use, your right to use the Website will cease immediately and you must, at our option, return or destroy any copies of the materials you have made. No right, title or interest in or to the Website or any content on the Website is transferred to you, and all rights not expressly granted are reserved by the Company. Any use of the Website not expressly permitted by these Terms of Use is a breach of these Terms of Use and may violate copyright, trademark and other laws.</p>

			<strong>Trademarks</strong>

			<p>The Company name, the Company logo and all related names, logos, product and service names, designs and slogans are trademarks of the Company or its affiliates or licensors. You must not use such marks without the prior written permission of the Company. All other names, logos, product and service names, designs and slogans on this Website are the trademarks of their respective owners.</p>

			<strong>Prohibited Uses</strong>

			<p>You may use the Website only for lawful purposes and in accordance with these Terms of Use. You agree not to use the Website:</p>

			<ul>
				<li>In any way that violates any applicable federal, state, local or international law or regulation (including, without limitation, any laws regarding the export of data or software to and from the US or other countries).</li>
				<li>For the purpose of exploiting, harming or attempting to exploit or harm minors in any way by exposing them to inappropriate content, asking for personally identifiable information or otherwise.</li>
				<li>To transmit, or procure the sending of, any advertising or promotional material [without our prior written consent], including any “junk mail”, “chain letter” or “spam” or any other similar solicitation.</li>
				<li>To impersonate or attempt to impersonate the Company, a Company employee, another user or any other person or entity (including, without limitation, by using e-mail addresses or screen names associated with any of the foregoing).</li>
				<li>To engage in any other conduct that restricts or inhibits anyone’s use or enjoyment of the Website, or which, as determined by us, may harm the Company or users of the Website or expose them to liability.</li>
			</ul>
			
			<p>Additionally, you agree not to:</p>

			<ul>
				<li>Use the Website in any manner that could disable, overburden, damage, or impair the site or interfere with any other party’s use of the Website, including their ability to engage in real time activities through the Website.</li>
				<li>Use any robot, spider or other automatic device, process or means to access the Website for any purpose, including monitoring or copying any of the material on the Website.</li>
				<li>Use any manual process to monitor or copy any of the material on the Website or for any other unauthorized purpose without our prior written consent.</li>
				<li>Use any device, software or routine that interferes with the proper working of the Website.</li>
				<li>Introduce any viruses, trojan horses, worms, logic bombs or other material which is malicious or technologically harmful.</li>
				<li>Attempt to gain unauthorized access to, interfere with, damage or disrupt any parts of the Website, the server on which the Website is stored, or any server, computer or database connected to the Website.</li>
				<li>Attack the Website via a denial-of-service attack or a distributed denial-of-service attack.</li>
				<li>Otherwise attempt to interfere with the proper working of the Website.</li>
			</ul>
			
			<strong>Changes to the Website</strong>

			<p>We may update the content on this Website from time to time, but its content is not necessarily complete or up-to-date. Any of the material on the Website may be out of date at any given time, and we are under no obligation to update such material.</p>

			<strong>Information About You and Your Visits to the Website</strong>

			<p>All information we collect on this Website is subject to our Privacy Policy. By using the Website, you consent to all actions taken by us with respect to your information in compliance with the Privacy Policy.</p>

			<strong>Online Purchases For brunchwork Services and Events</strong>

			<p>Tickets for Services and Events that are available through the Website are subject to additional terms that may be set forth on tickets or the pages or screens from which tickets are obtained, as well as any applicable rules and policies imposed by third party organizers, hosts, sponsors, or venues where Events are held. Tickets obtained or purchased from EventBrite, as further described herein, are also subject to the EventBrite Terms of Service Agreement and EventBrite Privacy Policy. Tickets obtained through any other source linked from the Site or otherwise affiliated with brunchwork are also subject to any terms and conditions and privacy policies that may be imposed by the organizer, venue, or ticketing service for such Events.</p>

			<p>Please contact us at support@brunchwork.com if you have any questions about the Website.</p>

			<strong>ORDERING PROCESS & PAYMENT METHODS</strong>

			<p>Tickets may be available through EventBrite or by linking to a third party organizer’s or venue’s website or ticketing service. Tickets available through EventBrite are subject to the EventBrite Terms of Service Agreement and EventBrite Privacy Policy. If applicable, payments for tickets are made to EventBrite through EventBrite’s payment processing. EventBrite accepts Visa, Mastercard, American Express, and Discover. These payment methods are subject to change.</p>

			<p>brunchwork has no control over EventBrite and is not responsible or liable if EventBrite or EventBrite’s payment process is unavailable for any reason, including but not limited to technical issues or other outages that may impact your ability to obtain or purchase tickets.</p>

			<p>Upon receipt of a credit card authorization from a ticket purchase, Eventbrite will email you a confirmation and issue you a unique confirmation number using the email address provided with your order. We are not responsible for issuing order confirmations or verifying the receipt of payments. You are responsible for confirming that your order has been processed and finalized.</p>

			<strong>MEMBERSHIP</strong>

			<p>When purchasing a membership, you agree to brunchwork and/or brunchwork’s third party payment processor storing Your payment card information. You also agree to pay the applicable Fees for the Premium Services (including, without limitation, periodic fees for premium accounts) as they become due plus all related taxes, and to reimburse us for all collection costs and interest for any overdue amounts. If You choose to cancel Your subscription as set out below, your obligation to pay Fees continues through the end of the then-current Subscription Period.</p>

			<p>All Premium Services subscriptions are offered for a six-month period (“Subscription Period”). You may choose to pay the Fees (i) in full in one payment when you register, or (ii) in equal installments on a monthly basis. If You choose the monthly payment option, You agree to pay the monthly Fees every month for the Subscription Period. Subscription Periods are automatically renewed as set forth below.</p>

			<p>When You register for membership, You hereby authorize us to charge Your payment card for the Fees. We will charge Your payment card for the amount of the Fees immediately on the 1st or 15th of the month. If You choose to pay for membership on a monthly basis, You authorize us to charge You for the monthly Fees every month until the expiration of Your Subscription Period, subject to earlier termination and/or renewal as set forth herein, to the payment method You provided when You registered.</p>

			<p>(a) This Agreement shall commence on the date You accept this Agreement, and shall continue (i) with respect to Your paid subscription as a brunchwork member, until the expiration of Your Subscription Period, subject to earlier termination and/or renewal as set forth herein and (ii) with respect to Your unpaid subscription as a brunchwork member, for so long as You are a user of the Services, subject to earlier termination as set forth herein (“Term”).</p>

			<p>(b) Subject to our right to suspend or terminate Your subscription, as described below, Your Subscription will automatically renew at the end of the Subscription Period, unless You cancel your subscription. We also reserve the right not to renew Your subscription in our discretion. Renewals are for the same duration as the original Subscription Period. By entering into this Agreement, You acknowledge that Your account will be subject to the above-described automatic renewals.</p>

			<p>(c) You may terminate this Agreement and Your subscription, where applicable, for any reason or no reason at all, by providing a written notice to brunchwork at concierge@brunchwork.com. In such case, Your subscription will terminate effective ten (10) days from the notice date, provided however, that You will not receive any refunds of any Fees paid to brunchwork, and, if you have elected for the monthly payment option, You will remain obligated to pay the monthly Fees for the remainder of the Subscription Period during which such termination becomes effective.</p>

			<strong>TICKETS</strong>

			<p>Unless otherwise indicated in the description for a particular Event, tickets will be emailed to you by EventBrite along with the confirmation you receive upon ordering tickets. It is your responsibility to bring copies of your tickets to the Event if necessary. For some Events, electronic tickets may be accepted. Please check the Event description for more information.</p>

			<p>We reserve the right to limit the number of tickets you may receive for Events. Any applicable ticket limits will be posted in the description for the Event.</p>

			<p>Tickets are not redeemable for cash or other value except admittance to the Event for which the tickets are issued. Subject to applicable law, Event tickets may not be resold without brunchwork’s prior consent.</p>

			<strong>PRIVACY</strong>

			<p>You acknowledge that payments for tickets are made to Eventbrite and that brunchwork will not receive, have access to, or store your credit card information. Any personal information you provide as part of the ticketing process that brunchwork receives, has access to, or stores shall be subject to and treated in accordance with the Privacy Policy.</p>

			<p>Tickets for some Events may be obtained through other sources linked from the Site. These other sources may use cookies, web beacons and other tracking technologies, collect data, and use data in ways that brunchwork would not. brunchwork is not responsible for the terms and conditions or privacy practices of any other source linked from the Website. Please visit the websites of these other sources if you wish to review their terms or privacy policies.</p>

			<strong>CANCELLATIONS OR CHANGES TO AN EVENT</strong>

			<p>We reserve the right to cancel, postpone, and/or change any Event for any reason at any time.</p>

			<p>If you obtained tickets through EventBrite for an Event that is cancelled or postponed, we will attempt to notify you using the email address used in connection with the ticket order. If you purchased tickets through EventBrite for an Event that has been cancelled, a full refund will automatically be issued through EventBrite (as described in the Refund Policy section below). We are not liable for partial performances, or venue, line-up, date, or time changes, and will not issue refunds in such cases.</p>

			<p>If you obtained tickets through a third party organizer’s or venue’s website or ticketing service that is cancelled or postponed, we will use reasonable efforts to notify you if we are informed that the Event is cancelled or postponed and if we have your contact information.</p>

			<strong>REFUND POLICY</strong>

			<p>Tickets are non-refundable except if an Event is cancelled as described above, or if you contact brunchwork at support@brunchwork.com more than seven (7) days prior to an Event to cancel your attendance. Please carefully confirm the Event name, time, location, and quantity of tickets before submitting your order.</p>
</p>
			<p>Where permitted, refunds will be issued through EventBrite to the method of payment used for the transaction within thirty days. When a refund is processed, you will receive an email notification from EventBrite.</p>

			<strong>PROMOTIONS & CONTESTS</strong>

			<p>Tickets are for the personal use of the purchaser only and may not be used for commercial purposes, advertising, promotions, lotteries, raffles, contests, or sweepstakes, without brunchwork’s prior written consent.</p>

			<strong>RECORDINGS & PHOTOGRAPHS</strong>

			<p>You may not record, publish, transmit, display, sell, or use live audio or video, recordings, or photographs of an Event for commercial purposes without prior written consent from brunchwork and/or the applicable rights holders. You may record, publish, transmit, display, and use a limited and reasonable amount of recordings or photographs of an Event for personal, non-commercial use to the extent permitted by applicable law, provided that (a) you do not use such materials in a manner that suggests brunchwork promotes or endorses your, or any third party’s, causes, ideas, websites, products, or services; (b) you do not use such materials in any way that is unlawful or harmful to any other person or entity, including but not limited to any use that defames or invades the privacy of any person or entity; (c) you agree to hold brunchwork harmless from any claims arising from such usage; and (d) you agree to comply with any takedown requests issued by brunchwork following brunchwork’s receipt of any notice of actual or alleged infringement by a third party rights holder or notice of actual or alleged defamation or other unlawful use. In addition, you agree to abide by any applicable rules and policies imposed by a third party organizer or venue where an Event is held related to recording or use of recordings of the Event, including but not limited to prohibitions against recording or photographing an Event.</p>

			<p>You acknowledge that Events are open to the public and that your appearance and actions inside and outside of the venue where an Event occur are public in nature, and you have no expectation of privacy with regard to your actions or conduct at Events. By attending an Event, you grant permission to brunchwork and its employees, agents, member stations, and licensees, and to any hosts, participants, third party organizers, and sponsors of the Event and their employees, agents, and licensees, permission to record, publish, transmit, distribute, display, and otherwise use your name, biographical information, image, likeness, appearance, acts, movements, and statements related to your attendance at or participation in the Event for any purpose, in any manner, in any medium or context now known or hereafter developed, including but not limited to news reporting, publicity, and promotional purposes, throughout the world, without further authorization from or compensation to you.</p>

			<strong>LINKS TO THIRD PARTY SITES AND SERVICES</strong>

			<p>The Website may include links to websites, applications, and services maintained by third parties, over which brunchwork has no control. brunchwork does not endorse the content, operators, products, or services of such sites, including but not limited to EventBrite, and brunchwork is not responsible for the content, operators, availability, accuracy, quality, advertising, data collection and privacy practices, data security, products, services, or other materials on or available from such sites.</p>

			<p>brunchwork shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, products, or services available on or through EventBrite or other such sites.</p>

			<strong>EJECTIONS & SEARCHES</strong>

			<p>We appreciate your cooperation with the rules and policies applicable to Events and the venues where Events are held, including those related to security measures for the safety of our audience and participants.</p>

			<p>We, together with any organizer, host, sponsor, venue, or other participant, reserve the right, without refund, to refuse admission to or eject any person whose conduct brunchwork, or any organizers, hosts, sponsors, or venues, deems disorderly, or who fails to comply with applicable rules and policies for an Event. When entering an age-restricted Event, appropriate identification must be shown. If suitable identification is not presented, you may be refused admittance to the Event without a refund.</p>

			<p>You and your belongings may be searched upon entry to an Event. You consent to such searches and waive any related claims that may arise. If you elect not to consent to such searches, you may be denied entry to the Event without a refund. We and certain venues operators may have additional rules and policies specific to certain Events related to searches and what items may be brought onto the premises, including but not limited to alcohol, drugs, weapons, cameras, and laser pointers. We reserve the right, without refund, to refuse admission to or eject any person who fails to comply with such rules and policies.</p>

			<strong>Linking to the Website and Social Media Features</strong>

			<p>You may link to our homepage, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it, but you must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part without our express written consent.</p>

			<p>This Website may provide certain social media features that enable you to:</p>

			<ul>
				<li>Link from your own or certain third-party websites to certain content on this Website.</li>
				<li>Send e-mails or other communications with certain content, or links to certain content, on this Website.</li>
				<li>Cause limited portions of content on this Website to be displayed or appear to be displayed on your own or certain third-party websites.</li>
			</ul>

			<p>You may use these features solely as they are provided by us, and solely with respect to the content they are displayed with. Subject to the foregoing, you must not:</p>

			<ul>
				<li>Establish a link from any website that is not owned by you.</li>
				<li>Cause the Website or portions of it to be displayed, or appear to be displayed by, for example, framing, deep linking or in-line linking, on any other site.</li>
				<li>Link to any part of the Website other than the homepage.</li>
				<li>Otherwise take any action with respect to the materials on this Website that is inconsistent with any other provision of these Terms of Use.</li>
			</ul>

			<p>You agree to cooperate with us in causing any unauthorized framing or linking immediately to cease. We reserve the right to withdraw linking permission without notice.</p>

			<p>We may disable all or any social media features and any links at any time without notice in our discretion.</p>

			<strong>Links from the Website</strong>

			<p>If the Website contains links to other sites and resources provided by third parties, these links are provided for your convenience only. This includes links contained in advertisements, including banner advertisements and sponsored links. We have no control over the contents of those sites or resources, and accept no responsibility for them or for any loss or damage that may arise from your use of them. If you decide to access any of the third party websites linked to this Website, you do so entirely at your own risk and subject to the terms and conditions of use for such websites.</p>
			
			<strong>Feedback</strong>

			<p>We may provide you with a mechanism to provide feedback, suggestions, and ideas, if you choose, about our Services (“Feedback”). By submitting any Feedback, you agree that your disclosure is gratuitous, unsolicited and without restriction and will not place us under any fiduciary or other obligation, and that we are free to use the Feedback without any additional compensation to you, and/or to disclose the Feedback on a non-confidential basis or otherwise to anyone. You further acknowledge that, by acceptance of your submission, the Company does not waive any rights to use similar or related ideas previously known to the Company, or developed by its employees, or obtained from sources other than you. You agree that we may, in our sole discretion, use the Feedback you provide to us in any way, including in future enhancements and modifications to our Services or Website. You hereby grant to us and our assigns a perpetual, worldwide, fully transferable, sublicensable, irrevocable, royalty free license to use, reproduce, modify, create derivative works from, distribute, and display the Feedback in any manner any for any purpose, without in any media, software, or technology of any kind now existing or developed in the future, without any obligation to provide attribution or compensation to you or any third party.</p>

			<strong>Geographic Restrictions</strong>

			<p>The owner of the Website is based in the state of New York in the United States. We provide this Website for use only by persons located in the United States. We make no claims that the Website or any of its content is accessible or appropriate outside of the United States. Access to the Website may not be legal by certain persons or in certain countries. If you access the Website from outside the United States, you do so on your own initiative and are responsible for compliance with local laws.</p>

			<strong>Disclaimer of Warranties</strong>

			<p>YOUR USE OF THE SERVICES AND THE SERVICE CONTENT, ALONG WITH ATTENDANCE AT ANY EVENTS, IS AT YOUR SOLE RISK. THE SERVICES AND THE SERVICE CONTENT EACH ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS. WE AND OUR SUPPLIERS AND LICENSORS EXPRESSLY DISCLAIM ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT. WE DO NOT GUARANTEE THE ACCURACY, COMPLETENESS, OR USEFULNESS OF THE SERVICES OR ANY SERVICE CONTENT, AND YOU RELY ON THE SERVICES AND SERVICE CONTENT AT YOUR OWN RISK. ANY MATERIAL THAT YOU ACCESS OR OBTAIN THROUGH OUR SERVICES IS DONE AT YOUR OWN DISCRETION AND RISK AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY MATERIAL THROUGH OUR SERVICES. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM US OR THROUGH OR FROM OUR SERVICES WILL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THESE TERMS.</p>

			<p>COMPANY DOES NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH THE SERVICE OR ANY HYPERLINKED WEBSITE OR SERVICE, AND COMPANY WILL NOT BE A PARTY TO OR IN ANY WAY MONITOR ANY TRANSACTION BETWEEN YOU AND THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES.</p>

			<p>THE FOREGOING DOES NOT AFFECT ANY WARRANTIES WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.</p>

			<strong>Limitation on Liability</strong>

			<p>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL THE COMPANY, ITS AFFILIATES OR THEIR LICENSORS, SERVICE PROVIDERS, EMPLOYEES, AGENTS, OFFICERS OR DIRECTORS BE LIABLE FOR DAMAGES OF ANY KIND, UNDER ANY LEGAL THEORY, ARISING OUT OF OR IN CONNECTION WITH YOUR USE, OR INABILITY TO USE, THE WEBSITE, ANY WEBSITES LINKED TO IT, ANY CONTENT ON THE WEBSITE OR SUCH OTHER WEBSITES OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE OR SUCH OTHER WEBSITES, INCLUDING ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, INCLUDING BUT NOT LIMITED TO, PERSONAL INJURY, PAIN AND SUFFERING, EMOTIONAL DISTRESS, LOSS OF REVENUE, LOSS OF PROFITS, LOSS OF BUSINESS OR ANTICIPATED SAVINGS, LOSS OF USE, LOSS OF GOODWILL, LOSS OF DATA, AND WHETHER CAUSED BY TORT (INCLUDING NEGLIGENCE), BREACH OF CONTRACT OR OTHERWISE, EVEN IF FORESEEABLE.</p>

			<p>You voluntarily assume all risk and danger incidental to your attendance and participation at any Event, whether occurring before, during, or after the Event, and you waive any claims for personal injury or death against brunchwork and its officers, directors, employees, or agents, and any organizers, hosts, sponsors, or venues for the Event, and their officers, directors, employees, or agents, on behalf of yourself and any accompanying minor.</p>

			<p>THE FOREGOING DOES NOT AFFECT ANY LIABILITY WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.</p>

			<strong>Indemnification</strong>

			<p>You agree to defend, indemnify, and hold harmless the Company, its affiliates, licensors and service providers, and its and their respective officers, directors, employees, contractors, agents, licensors, suppliers, successors and assigns from and against any claims, liabilities, damages, judgments, awards, losses, costs, expenses or fees (including reasonable attorneys’ fees) arising out of or relating to your violation of these Terms of Use or your use of the Website, including, but not limited to, any use of the Website’s content, Services, Events and products other than as expressly authorized in these Terms of Use or your use of any information obtained from the Website. If at any time you are not happy with the Website or the Service or object to any material within the Website or the Service, your sole remedy is to cease using them.</p>

			<strong>Governing Law and Jurisdiction</strong>

			<p>All matters relating to the Website and these Terms of Use and any dispute or claim arising therefrom or related thereto (in each case, including non-contractual disputes or claims), shall be governed by and construed in accordance with the internal laws of the State of New York without giving effect to any choice or conflict of law provision or rule (whether of the State of New York or any other jurisdiction).</p>

			<p>Any legal suit, action or proceeding arising out of, or related to, these Terms of Use or the Website shall be instituted exclusively in the federal courts of the United States in the Southern District of New York or the courts of the State of New York although we retain the right to bring any suit, action or proceeding against you for breach of these Terms of Use in your country of residence or any other relevant country. You waive any and all objections to the exercise of jurisdiction over you by such courts and to venue in such courts.</p>

			<strong>Arbitration</strong>

			<p>At the Company’s sole discretion, it may require You to submit any disputes arising from the use of these Terms of Use or the Website, including disputes arising from or concerning their interpretation, violation, invalidity, non-performance, or termination, to final and binding arbitration under the Rules of Arbitration of the American Arbitration Association applying Wisconsin law. ALL CLAIMS MUST BE BROUGHT IN THE PARTIES’ INDIVIDUAL CAPACITY, AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY PURPORTED CLASS OR REPRESENTATIVE PROCEEDING. YOU AGREE THAT, BY ENTERING INTO THIS AGREEMENT, YOU AND THE COMPANY ARE EACH WAIVING THE RIGHT TO A TRIAL BY JURY OR TO PARTICIPATE IN A CLASS ACTION.</p>

			<strong>Limitation on Time to File Claims</strong>

			<p>ANY CAUSE OF ACTION OR CLAIM YOU MAY HAVE ARISING OUT OF OR RELATING TO THESE TERMS OF USE OR THE WEBSITE MUST BE COMMENCED WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION ACCRUES, OTHERWISE, SUCH CAUSE OF ACTION OR CLAIM IS PERMANENTLY BARRED.</p>

			<strong>Waiver and Severability</strong>

			<p>No waiver of by the Company of any term or condition set forth in these Terms of Use shall be deemed a further or continuing waiver of such term or condition or a waiver of any other term or condition, and any failure of the Company to assert a right or provision under these Terms of Use shall not constitute a waiver of such right or provision.</p>

			<p>If any provision of these Terms of Use is held by a court or other tribunal of competent jurisdiction to be invalid, illegal or unenforceable for any reason, such provision shall be eliminated or limited to the minimum extent such that the remaining provisions of the Terms of Use will continue in full force and effect.</p>

			<strong>Assignment</strong>

			<p>This Agreement, and any rights and licenses granted hereunder, may not be transferred or assigned by you, but may be assigned by the Company without restriction. Any attempted transfer or assignment in violation hereof shall be null and void.</p>

			<strong>Entire Agreement</strong>

			<p>The Terms of Use and our Privacy Policy constitute the sole and entire agreement between you and brunchwork with respect to the Website, Services and Events, and supersede all prior and contemporaneous understandings, agreements, representations and warranties, both written and oral, with respect to the Website, Services and Events.</p>

			<strong>Your Comments and Concerns</strong>

			<p>This website is operated by brunchwork. All feedback, comments, requests for technical support and other communications relating to the Website should be directed to: support@brunchwork.com</p>
		</div>
	</div>
	<div class="col-md-4"></div>
</div>
<div class="hundred-spacer"></div>