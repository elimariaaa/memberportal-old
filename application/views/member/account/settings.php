<link href="<?php echo base_url('assets/css/card-js.min.css?v='.VER_NO); ?>" rel="stylesheet">
<script src="https://js.braintreegateway.com/web/dropin/1.13.0/js/dropin.min.js"></script>
<script src="https://js.braintreegateway.com/web/3.38.0/js/client.min.js"></script>
<div class="twenty-spacer"></div>
<h1>Settings</h1>
<div class="card-deck">
	<div class="card col-md-8 no-radius">
		<div class="card-body">
			<h5 class="card-title">Member Plan</h5>
			<hr />
			<table class="table borderless">
				<thead>
					<tr class="row">
						<th class="col">Plan Type</th>
						<th class="col">Renewal Date</th>
					</tr>
				</thead>
				<tbody>
					<tr class="row">
						<td class="col"><?php echo $subscriber_plan; ?></td>
						<td class="col"><?php echo $renewal_date; ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="card col-md-4 no-radius buttons-card">
		<div class="list-group list-group-flush">
			<a href="#" id="sidebarCollapse" class="list-group-item list-group-item-action"><i class="far fa-user-circle"></i> Edit Profile</a>
			<a href="#" class="list-group-item list-group-item-action"><i class="far fa-question-circle"></i> Support</a>
			<a href="#" class="list-group-item list-group-item-action"><i class="far fa-list-alt"></i> Member Guidelines</a>
		</div>
	</div>
</div>
<div class="fifty-spacer"></div>
<div class="card-deck">
	<div class="card col-md-8 no-radius">
		<div class="card-body">
			<h5 class="card-title">Card Information</h5>
			<hr />
			<div class="cc_alert"></div>
			<!--
			<div id="dropin-container"></div>
			<div class="text-center"><button type="button" class="btn btn-brunchwork btn-lg" id="submit-button">Add/Update Card</button></div>
			<script>
				var button = document.querySelector('#submit-button');

				braintree.dropin.create({
				authorization: '<?php echo $client_token; ?>',
				container: '#dropin-container',
				defaultFirst: true,
				vaultManager: true
				}, function (createErr, instance) {
				button.addEventListener('click', function () {
					instance.requestPaymentMethod(function (err, payload) {
					// Submit payload.nonce to your server
					});
				});
				});
			</script>
			-->
			
			<form id="credit_card_form">
				<div class="card-js"></div>
				<div class="twenty-spacer"></div>
				<?php
					$csrf = array(
							'name' => $this->security->get_csrf_token_name(),
							'hash' => $this->security->get_csrf_hash()
						);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<div class="text-center"><button type="button" class="btn btn-brunchwork btn-lg" id="credit_card_btn">Update Card</button></div>
			</form>
			
		</div>
	</div>
	<div class="card col-md-4 empty bg-pink"></div>
</div>
<div class="fifty-spacer"></div>
<div class="card-deck">
	<div class="card col-md-8 no-radius">
		<div class="card-body">
			<h5 class="card-title">Account Email</h5>
			<hr />
			<div class="update_email_alert"></div>
			<form id="email_form">
			<input type="email" class="form-control no-radius" name="update_email" value="<?php echo $email_address; ?>" />
			<div class="twenty-spacer"></div>
			<?php
				$csrf = array(
						'name' => $this->security->get_csrf_token_name(),
						'hash' => $this->security->get_csrf_hash()
					);
			?>
			<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
			<div class="text-center"><button type="button" class="btn btn-brunchwork btn-lg" id="email_btn">Update Email</button></div>
			</form>
		</div>
	</div>
	<div class="card col-md-4 empty bg-pink"></div>
</div>
<div class="fifty-spacer"></div>
<div class="card-deck">
	<div class="card col-md-8 no-radius">
		<div class="card-body">
			<h5 class="card-title">Mailing Address</h5>
			<hr />
			<div class="update_address_alert"></div>
			<form id="mailing_address_form">

				<div class="form-group"> <!-- Street 1 -->
					<label for="street_address" class="control-label">Street Address</label>
					<input type="text" class="form-control no-radius" id="street_address" name="street" placeholder="Street address, P.O. box, company name, c/o" value="<?php echo ($street_address != '') ? $street_address:''; ?>">
				</div>					
										
				<div class="form-group"> <!-- Street 2 -->
					<label for="extended_address" class="control-label">Extended Address</label>
					<input type="text" class="form-control no-radius" id="extended_address" name="extended" placeholder="Extended Address" value="<?php echo ($extended_address != '') ? $extended_address:''; ?>">
				</div>	

				<div class="form-group"> <!-- City-->
					<label for="city_id" class="control-label">City (Locality)</label>
					<input type="text" class="form-control no-radius" id="city_id" name="city" placeholder="City (Locality)" value="<?php echo ($locality != '') ? $locality:''; ?>">
				</div>									
										
				<div class="form-group"> <!-- State Button -->
					<label for="state_id" class="control-label">State/Province (Region)</label>
					<input type="text" class="form-control no-radius" id="state_id" name="state" placeholder="State/Province (Region)" value="<?php echo ($region != '') ? $region:''; ?>">
					<!--
					<select class="form-control no-radius" id="state_id">
						<?php echo $states; ?>
					</select>					
					-->
				</div>
				
				<div class="form-group"> <!-- Zip Code-->
					<label for="postal_code" class="control-label">Postal Code</label>
					<input type="text" class="form-control no-radius" id="postal_code" name="postal" placeholder="Postal Code" value="<?php echo ($postalCode != '') ? $postalCode:''; ?>">
				</div>

				<div class="form-group"> <!-- Country -->
					<label for="country_name" class="control-label">Country</label>
					<select name="country" id="country_options" class="form-control">
						<option value='' selected disabled>Select country.</option>
						<?php
							foreach($countries AS $get_countries){
								if($countryName == $get_countries['country_name']){
									$selected = 'selected';
								} else {
									$selected = '';
								}
								echo "<option data-alpha2='".$get_countries['alpha2']."' data-alpha3='".$get_countries['alpha3']."' data-numeric='".$get_countries['country_numeric']."' value='".$get_countries['country_name']."' ".$selected.">".$get_countries['country_name']."</option>";
							}
							
						?>
					</select>
				</div>
				<div class="twenty-spacer"></div>
				<?php
					$csrf = array(
						'name' => $this->security->get_csrf_token_name(),
						'hash' => $this->security->get_csrf_hash()
					);
				?>
				<input type="hidden" id="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<input type="hidden" id="address_id" name="address_id" value="<?php echo ($address_id != '') ? $address_id:''; ?>" />
				<div class="text-center"><button type="button" class="btn btn-brunchwork btn-lg" id="add_address"><?php echo $address_button; ?> Address</button></div>
			</form>
		</div>
	</div>
	<div class="card col-md-4 empty bg-pink"></div>
</div>
<div class="fifty-spacer"></div>

<div class="card-deck">
	<div class="card col-md-8 no-radius">
		<div class="card-body">
			<h5 class="card-title">Recent Invoice History</h5>
			<hr />
			<table class="table borderless">
				<thead>
					<tr class="row">
						<th class="col">ID</th>
						<th class="col">Payment Date</th>
						<th class="col">Amount</th>
						<th class="col">Payment Status</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						if(!$transactions || count($transactions->_ids) == 0){
							$creditcard_token = '';
					?>
					<tr class="row">
						<td width="100%" colspan="4" class="text-center">No data found.</td>
					</tr>
					<?php
					} else {
						$max_count = 1;
						$counter = 0;
						if(count($transactions->_ids) > 0){
						foreach($transactions AS $get_transactions){
							if($counter == 0){
							}
							if($cc_bin == $get_transactions->creditCard['bin']){
								$creditcard_token = $get_transactions->creditCard['token'];
							}
							if($counter < $max_count){
								$currency = $get_transactions->currencyIsoCode;
								$payment_id = $get_transactions->id;
								$payment_date = $get_transactions->createdAt->format('F j, Y');
								$payment_amount = $get_transactions->amount;
								$payment_status = $get_transactions->status;

						
					?>
					<tr class="row">
						<td class="col"><?php echo $payment_id; ?></td>
						<td class="col"><?php echo $payment_date; ?></td>
						<td class="col"><?php echo number_format($payment_amount, 2).' '.$currency; ?></td>
						<td class="col"><?php echo ucfirst($payment_status); ?></td>
					</tr>
					<?php	
							}
							$counter++;
						}
					} 
					} ?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="card col-md-4 empty bg-pink"></div>
</div>
<?php
	echo "<input type='hidden' name='cc_token' id='cc_token' value='".$token."' />";
	echo "<input type='hidden' name='cc_bin' id='cc_bin' value='".$cc_bin."' />";
	echo "<input type='hidden' name='customer_braintree_id' id='customer_braintree_id' value='".$customer_braintree_id."' />";
?>
<script>
	$(document).ready(function(){

	});
</script>
<div class="hundred-spacer"></div>
<script type="text/javascript" src="<?php echo base_url('assets/js/card-js.min.js?V='.VER_NO);?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/settings.js?V='.VER_NO);?>"></script>