<?php
    $user = $this->ion_auth->user()->row();
    //print_r($user);
?>

<div class="twenty-spacer"></div>
<div class="container">
    <h1>Membership</h1>
    <div class="mb-5"></div>
    <div class="padding-topbottom container">
        <div class="row">
        <div class="col-md-12 membership_alert">
        <?php 
            if($user->is_paused == 1){
        ?>
            <div class="alert alert-warning" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            You have successfully paused your membership for 3 months.
            </div>
        <?php
             } else if ($user->is_paused == 0 && $user->is_paused != NULL) {
        ?>
            <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Congratulations! You have successfully continued with your membership.
            </div>
        <?php
            }
        ?>
        </div>
        <div class="col-md-6 text-center">
        <?php
            if(strtolower($user->is_paused) == 1){
        ?>
        <button type="button" class="btn btn-brunchwork btn-lg unpause-membership block">Continue With Membership</button>
        <?php
            } else {
        ?>
        <button type="button" class="btn btn-brunchwork btn-lg pause-membership block">Pause Membership</button>
        <?php
            }
        ?>
        </div>
        <div class="col-md-6 text-center">
        <button type="button" class="btn btn-brunchwork btn-lg cancel-membership block">Cancel Membership</button>
        </div>
        </div>
    </div>

</div>
<div class="hundred-spacer"></div>
<script>
    var first_bill_date = '<?php echo $user->first_bill_date; ?>';
</script>
<script src="<?php echo base_url('assets/js/bootbox.min.js?v=').VER_NO; ?>"></script>
<script src="<?php echo base_url('assets/js/membership.js?v=').VER_NO; ?>"></script>