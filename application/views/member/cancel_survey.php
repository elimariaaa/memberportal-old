<?php
    $user = $this->ion_auth->user()->row();
    //print_r($user);
?>

<div class="twenty-spacer"></div>
<div class="container">
    <h1>Exit Survey</h1>
    <p class="text-left">Thank you for taking the time to helping us make brunchwork a better experience for future members! </p>
    <div class="padding-topbottom container">
        <div class="row">
        <div class="col-md-12 text-left mb4">
        <form id="cancel_survey">
            <fieldset class="form-group">
                <div class="row">
                <label for="exampleFormControlInput1">Why did you cancel your membership?</label>
                <!--<legend class="col-form-label col-sm-2 pt-0">Radios</legend>-->
                <div class="col-sm-12">
                    <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="too_expensive" id="too_expensive" value="1">
                    <label class="form-check-label" for="gridRadios1">
                        Too Expensive
                    </label>
                    </div>
                    <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="time_constraints" id="time_constraints" value="1">
                    <label class="form-check-label" for="gridRadios2">
                        Time constraints
                    </label>
                    </div>
                    <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="joined_other_org" id="joined_other_org" value="1">
                    <label class="form-check-label" for="gridRadios3">
                        Joined another organization
                    </label>
                    </div>
                    <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="event_quality" id="event_quality" value="1">
                    <label class="form-check-label" for="gridRadios3">
                        Event quality
                    </label>
                    </div>
                    <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="speaker_selection" id="speaker_selection" value="1">
                    <label class="form-check-label" for="gridRadios3">
                        Speaker selection
                    </label>
                    </div>
                </div>
                </div>
            </fieldset>
            <fieldset class="form-group">
                <div class="row">
                <label for="exampleFormControlTextarea1">If you don't see your reason above, what is it?</label>
                <div class="col-sm-12">
                <textarea class="form-control" id="other_reason_to_cancel" rows="3" name="other_reason_to_cancel"></textarea>
                </div>
                </div>
            </fieldset>
            <fieldset class="form-group">
                <div class="row">
                <label for="exampleFormControlTextarea1">What did you like about brunchwork?</label>
                <div class="col-sm-12">
                <textarea class="form-control" id="like_about_brunchwork" rows="3" name="like_about_brunchwork"></textarea>
                </div>
                </div>
            </fieldset>
            <fieldset class="form-group">
                <div class="row">
                <label for="exampleFormControlTextarea1">How can we improve the membership experience?</label>
                <div class="col-sm-12">
                <textarea class="form-control" id="improve_membership_exp" rows="3" name="improve_membership_exp"></textarea>
                </div>
                </div>
            </fieldset>
            <fieldset class="form-group">
                <div class="row">
                <label for="exampleFormControlTextarea1">What kind of events or perks do you wish we had offered?</label>
                <div class="col-sm-12">
                <textarea class="form-control" id="perks_to_offer" rows="3" name="perks_to_offer"></textarea>
                </div>
                </div>
            </fieldset>
            <fieldset class="form-group">
                <div class="row">
                <label for="exampleFormControlTextarea1">Anything else we should know?</label>
                <div class="col-sm-12">
                <textarea class="form-control" id="others" rows="3" name="others"></textarea>
                </div>
                </div>
            </fieldset>
            <fieldset class="form-group">
                <div class="row">
                <div class="col-sm-12">
                    <?php
                        $csrf = array(
                            'name' => $this->security->get_csrf_token_name(),
                            'hash' => $this->security->get_csrf_hash()
                        );
                    ?>
                    <input type="hidden" id="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                </div>
                </div>
            </fieldset>
            <div class="col-md-12 text-center">
            <button type="button" class="btn btn-brunchwork btn-lg submit-survey">Submit</button>
            <button type="button" class="btn btn-brunchwork btn-lg skip-survey">Skip</button>
            </div>
        </form>
        </div>
        </div>
    </div>

</div>
<div class="hundred-spacer"></div>
<script>
    var first_bill_date = '<?php echo $user->first_bill_date; ?>';
</script>
<script src="<?php echo base_url('assets/js/bootbox.min.js?v=').VER_NO; ?>"></script>
<script src="<?php echo base_url('assets/js/survey.js?v=').VER_NO; ?>"></script>