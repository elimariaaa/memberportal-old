<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- favicon -->
		<link rel="icon" href="<?php echo base_url('assets/images/fav-icon.png'); ?>">
		<!-- jQuery CDN -->
		<script  src="https://code.jquery.com/jquery-3.3.1.min.js"  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="  crossorigin="anonymous"></script>
		<!-- jQuery local fallback -->
		<script>window.jQuery || document.write('<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js'); ?>"><\/script>')</script>
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

		<!-- Custom styles for signin template -->
		<link href="<?php echo base_url('assets/css/members/signin.css?v='.VER_NO); ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/members/styles.css?v='.VER_NO); ?>" rel="stylesheet">
		<title><?php echo $pagetitle;?></title>
		<script type="text/javascript"> window.base_url = '<?php echo base_url(); ?>';</script>
  	</head>
	<body>