		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<!-- Bootstrap JS local fallback -->
		<script>if(typeof($.fn.modal) === 'undefined') {document.write('<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"><\/script>')}</script>
		<!-- Bootstrap CSS local fallback -->
		<div id="bootstrapCssTest" class="hidden"></div>
		<script>
			$(function() {
				if ($('#bootstrapCssTest').is(':visible')) {
					$("head").prepend('<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">');
				}
			});
		</script>
		<footer class="footer my-5 pt-5 text-muted text-center text-small">
			<p class="mb-1">&copy; 2018 Brunchwork</p>
			<!--
			<ul class="list-inline">
				<li class="list-inline-item"><a href="#">Privacy</a></li>
				<li class="list-inline-item"><a href="#">Terms</a></li>
				<li class="list-inline-item"><a href="#">Support</a></li>
			</ul>
			-->
		</footer>
	</body>
</html>