<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'member/login/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

//login
$route['login'] = 'member/login/login';
//logout
$route['logout'] = 'member/login/logout';
//forgot password
$route['forgot_password'] = 'member/login/forgot_password';
//reset password email
$route['reset_password'] = 'member/Login/reset_password';
//dashboard
$route['dashboard'] = 'member/dashboard';
//directory
$route['directory'] = 'member/mdirectory';
$route['member/loadRecord'] = 'member/mdirectory/loadRecord';
//events
$route['events'] = 'member/events';
$route['events/authenticate'] = 'member/events/authenticate_user';
//privacy policy
$route['privacy_policy'] = 'member/user/privacy_policy';
//terms and conditions
$route['terms'] = 'member/user/terms';
//account settings
$route['settings'] = 'member/dashboard/settings';
//membership
$route['membership'] = 'member/membership';
$route['member/cancel'] = 'member/membership/cancel_membership';
$route['member/get_cities'] = 'member/user/get_cities';
$route['member/cancel_subscription'] = 'member/user/cancel_subscription';
$route['member/pause_subscription'] = 'member/user/pause_subscription';
$route['member/unpause_subscription'] = 'member/user/unpause_subscription';
$route['member/confirm_cancel'] = 'member/user/confirm_cancel';
$route['member/submit_survey'] = 'member/user/submit_survey';

//submit credit card
$route['user/submit_credit_card'] = 'member/user/submit_credit_card';
$route['user/update_email'] = 'member/user/update_email';
$route['user/update_address'] = 'member/user/update_address';
$route['user/update_profile'] = 'member/user/update_profile';
$route['user/get_members'] = 'member/user/get_members';
$route['user/contact_member'] = 'member/user/contact_member';
$route['braintree'] = 'member/dashboard/get_token';
$route['braintree/clients'] = 'member/dashboard/get_clients';
$route['braintree/client_data'] = 'member/dashboard/get_client_data';
$route['braintree/subscriptions/active'] = 'member/dashboard/active_subscriptions';
