<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Mdirectory extends Auth_Controller {
 
    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->model('UserModel');
        $this->load->model('MemberModel');
        $this->load->library("braintree_lib");
        // Load Pagination library
		$this->load->library('pagination');
    }
    
    public function index($rowno=0)
    {
        $user = $this->ion_auth->user()->row();
        if($this->ion_auth->logged_in()  && $user->is_paused != 1){
            if($user->is_paused != NULL){
                $data = array(
                    'is_paused' => NULL,
                );
                $this->ion_auth->update($user->id, $data);
            }
            $this->data['pagetitle'] = 'brunchwork | Directory';
            //$this->data['active_members'] = $this->braintree_lib->active_subscriptions();
            //$this->data['active_members'] = $this->MemberModel->get_members();
            $this->data['get_all_plans'] = $this->braintree_lib->get_all_plans();
            $this->data['cities'] = $this->MemberModel->get_all_city();
            $this->render('member/directory');
        } else {
            if($this->ion_auth->logged_in()  && $user->is_paused == 1){
                redirect('membership');
            } else {
                redirect('login');
            }
        }
    }

    public function loadRecord($rowno=0){
        
        // Row per page
        $rowperpage = 15;
    
        // Row position
        if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
        }
     
        // All records count
        $allcount = $this->MemberModel->getrecordCount();
    
        // Get records
        $users_record = $this->MemberModel->get_members($rowno,$rowperpage);
     
        // Pagination Configuration
        $config['base_url'] = base_url().'directory/loadRecord';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;

        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        // Initialize
        $this->pagination->initialize($config);
    
        // Initialize $data Array
        $data['pagination'] = $this->pagination->create_links();
        $data['result'] = $users_record;
        $data['row'] = $rowno;
    
        echo json_encode($data);
     
      }

    function get_address(){
        $this->load->library("braintree_lib");
        $address = $this->braintree_lib->member_address($this->input->get('braintree_id'));

        if($address->addresses){
            $data = array(
                'street_address' => $address->addresses[0]->streetAddress,
                'extended_address' => $address->addresses[0]->extendedAddress,
                'locality' => $address->addresses[0]->locality,
                'postalcode' => $address->addresses[0]->postalCode,
                'countrycode' => $address->addresses[0]->countryCodeAlpha3
            );
        } else {
            $data = array(
                'street_address' => '',
                'extended_address' => '',
                'locality' => '',
                'postalcode' => '',
                'countrycode' => ''
            );
        }
        echo json_encode($data);
    }

    function contact_member(){
        $data['to'] = ($this->input->post('email'));
        $this->load->view('member/contact_member_form', $data);
    }
}