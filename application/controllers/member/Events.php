<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Events extends Auth_Controller {
 
    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->model('UserModel');
        $this->load->model('MemberModel');
        $this->load->library("braintree_lib");
        // Load Pagination library
		$this->load->library('pagination');
    }
    
    public function index()
    {
        $user = $this->ion_auth->user()->row();
        if($this->ion_auth->logged_in()  && $user->is_paused != 1){
            if($user->is_paused != NULL){
                $data = array(
                    'is_paused' => NULL,
                );
                $this->ion_auth->update($user->id, $data);
            }
            $this->data['pagetitle'] = 'brunchwork | Events';
            if($this->session->flashdata('message')){
                $msg = $this->session->flashdata('message');
                $this->data['message'] = $msg['message'];
                $this->data['alert_type'] = $msg['alert_type'];
            } else {
                $this->data['message'] = '';
                $this->data['alert_type'] = '';
            }
            $this->render('member/events');
        } else {
            if($this->ion_auth->logged_in()  && $user->is_paused == 1){
                redirect('membership');
            } else {
                redirect('login');
            }
        }
    }

    function authenticate_user(){
        if($this->ion_auth->logged_in()) {
            $code = $this->input->get('code');
            $post_url = "https://www.eventbrite.com/oauth/token";
            $params = array('code' => $code, 'client_secret' => EVENTBRITE_CLIENT_SECRET, 'client_id' => EVENTBRITE_APP_KEY, 'grant_type' => 'authorization_code');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
            curl_setopt($ch, CURLOPT_URL, $post_url);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $response = curl_exec($ch);
            curl_close($ch);
            
            //{"access_token":"W5QQ4XOTFYQXRCO4CG7C","token_type":"bearer"}
            //stdClass Object ( [access_token] => CG5ZO2GF7UJPM2J653I7 [token_type] => bearer )
            $json_data = json_decode($response);
            
            $token = $json_data->access_token;
            $update_data_arr = array('eventbrite_token' => $token);
            $push_token = $this->UserModel->update_user($update_data_arr);
            if($push_token){
                $_SESSION['message'] = 'You have successfully authorized our Eventbrite App.';
                $this->session->mark_as_flash(array('message' => $_SESSION['message'], 'alert_type' => 'success'));
                redirect('events');
            }
        } else {
            redirect('login');
        }
    }
}