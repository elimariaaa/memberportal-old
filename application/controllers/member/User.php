<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class User extends MY_Controller {
	
	function __construct()
    {
        parent::__construct();
		$this->load->library('ion_auth');
		$this->load->model('UserModel');
	}
 
    public function index()
    {
        $this->load->view('welcome_message');
    }
 
    public function login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() === FALSE)
        {
	        $this->render('member/login');
        } else
        {
            $remember = (bool) $this->input->post('remember');
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            
            if ($this->ion_auth->login($username, $password, $remember))
            {
                redirect('dashboard');
            }
            else
            {
                $_SESSION['auth_message'] = $this->ion_auth->errors();
                $this->session->mark_as_flash('auth_message');
                redirect('user/login');
            }
        }
    }
 
    public function logout()
    {
        echo 'here we will do the logout';
    }

    function get_cities(){
        $data['cities'] = $this->UserModel->get_cities();
        echo json_encode($data);
    }

    function privacy_policy(){
        $this->data['pagetitle'] = 'Brunchwork | Privacy';
        $this->render('member/account/privacy_policy');
    }

    function terms(){
        $this->data['pagetitle'] = 'Brunchwork | Terms';
        $this->render('member/account/terms');
    }

    function submit_credit_card(){
        /*
        Array
        (
            [cardnumber] => 4111 1111 1111 1111
            [expiry] => 12 / 38
            [expiry_month] => 12
            [expiry_year] => 38
            [cvc] => 123
            [cc_token] => f8zfjcw
            [cc_bin] => 548009
        )
        */
        $this->load->library("braintree_lib");
        $credit_card_result = '';
        $data = array();
        $data['result'] = false;
        $data['alert_type'] = 'danger';
        $data['message'] = '<ul class="alert-list">';
        if($this->ion_auth->logged_in()){
            //check if the post data are not empty first
            if(!$this->input->post('cardnumber') || !$this->input->post('expiry') || !$this->input->post('cvc')){
                $data['message'] .= '<li>Please complete all the input fields.</li>';
            } else {
                $cardnumber = preg_replace('/\s+/', '', $this->input->post('cardnumber'));
                $expiry_month = $this->input->post('expiry_month');
                $expiry_year = $this->input->post('expiry_year');
                $cvc = $this->input->post('cvc');
                //check if card number is not empty and 16 digits
                if(!$cardnumber || strlen($cardnumber) != 16){
                    $data['message'] .= '<li>Your card number should contain 16 digits.</li>';
                }
                //check if expiry month is not empty, only 2 digits, and not exceeding to 12
                if(!$expiry_month || $expiry_month < 1 || $expiry_month > 12){
                    $data['message'] .= '<li>Please enter the right expiry month.</li>';
                }
                //check if expiry year is not empty and only 2 digits
                if(!$expiry_year || strlen($expiry_year) != 2){
                    $data['message'] .= '<li>Please enter the right expiry year.</li>';
                }
                //check if CVC is not empty and only 3 digits
                if(!$cvc || strlen($cvc) != 3){
                    $data['message'] .= '<li>Your CVC should be valid.</li>';
                }
                if(($cardnumber && strlen($cardnumber) == 16) && ($expiry_month && $expiry_month >= 1 && $expiry_month <= 12) && ($expiry_year && strlen($expiry_year) == 2) && ($cvc && strlen($cvc) == 3)){
                    /*
                    //insert/update the data in the database
                    $credit_card_result = $this->UserModel->submit_credit_card($cardnumber, $expiry_month, $expiry_year, $cvc);
                    */
                    //update the data in the braintree
                    $year = date_create_from_format('y', html_escape($expiry_year));
                    $token = html_escape($this->input->post('cc_token'));
                    $customer_braintree_id = html_escape($this->input->post('customer_braintree_id'));
                    $credit_card_arr = array(
                        'token' => $token,
                        'cvv' => html_escape($cvc),
                        'number' => html_escape($cardnumber),
                        //'expiration_date' => $expiry_month.'/'.$year->format('Y')
                        'expirationMonth' => $expiry_month,
                        'expirationYear' => $expiry_year

                    );
                    
                    $credit_card_result = $this->braintree_lib->create_cc($customer_braintree_id, $credit_card_arr);
                    if($credit_card_result && $credit_card_result != 'false'){
                        $data['result'] = true;
                        $data['alert_type'] = 'success';
                        $data['message'] .= '<li>Your credit card details have been successfully saved.</li>';
                    } else {
                        $data['message'] .= '<li>Something went wrong. Please try again.</li>';
                    }
                }
            }
            $data['message'] .= '</ul>';
            echo json_encode($data);
        } else {
            $array_message = array('message' => preg_replace("/[#]/", "", $this->ion_auth->errors()), 'alert_type' => 'danger');
			$this->session->set_flashdata('message', $array_message);
            redirect('login');
        }
    }

    function update_email(){
        $data = array();
        $data['result'] = false;
        $data['alert_type'] = 'danger';
        if($this->input->post() && $this->input->post('update_email') != ''){
            $update_email = $this->UserModel->update_email();
            if($update_email && $update_email['success'] == true){
                $data['result'] = true;
                $data['alert_type'] = 'success';
                $data['message'] = 'Your email has been successfully updated. You may login using this email address.';
            } else if($update_email && $update_email['success'] == false){
                $data['message'] = $update_email['message'];
            } else {
                $data['message'] = 'Email update failed. Please try again later.';
            }
        } else {
            $data['message'] = 'Please enter a valid email.';
        }

        echo json_encode($data);
    }

    function update_address(){
        $this->load->library("braintree_lib");
        $data = array();
        $data['result'] = false;
        $data['alert_type'] = 'danger';
        $customer_address = $this->braintree_lib->customer_address($this->input->post());
        $data['result'] = $customer_address['success'];
        if($customer_address['success'] == true){
            $data['alert_type'] = 'success';
            if($customer_address['action'] == 'save'){
                $data['message'] = 'Your address has been successfully saved.';
            }
            if($customer_address['action'] == 'update'){
                $data['message'] = 'Your address has been successfully updated.';
            }
        } else {
            if($customer_address['action'] == 'save'){
                $data['message'] = 'Saving address not successful. Please try again later.';
            }
            if($customer_address['action'] == 'update'){
                $data['message'] = 'Update address not successful. Please try again later.';
            }
        }
        echo json_encode($data);
    }

    function update_profile(){
        $data = array();
        $data['result'] = false;
        $data['success'] = false;
        $data['mesage'] = '';
        $data['alert_type'] = 'danger';
        if($this->ion_auth->logged_in()){
            $update_profile = $this->UserModel->update_profile();
            if($update_profile){
                $data['success'] = true;
                $data['alert_type'] = 'success';
                $data['message'] = "Update profile successful.";
            } else {
                $data['message'] = "Something went wrong. Please try again.";
            }
            echo json_encode($data);
        } else {
            redirect('login');
        }
    }

    function get_members(){
        $get_members = $this->UserModel->get_members();
        echo json_encode($get_members);
    }

    function contact_member(){
        $data = array();
        $data['success'] = false;
        $data['message'] = '';
        $data['alert_type'] = 'danger';

        if($this->ion_auth->logged_in()){
            if($this->input->post()){
                if($this->input->post('from_email') != '' && $this->input->post('to_email') != '' && $this->input->post('subject_email') != '' && $this->input->post('body_email') != ''){
                    $user = $this->ion_auth->user()->row();
                    $this->email->from('noreply@brunchwork.com', 'brunchwork Member Portal');
                    $this->email->to($this->input->post('to_email'));
                    $this->email->reply_to($this->input->post('from_email'), ucfirst($user->first_name).' '.ucfirst($user->last_name));
                    $this->email->subject('brunchwork Member Portal - '.$this->input->post('subject_email'));
                    $this->email->message(nl2br($this->input->post('body_email')));
                    if ( ! $this->email->send())
                    {
                        $data['message'] = 'Email not sent. '.$this->email->print_debugger();
                    } else {
                        $data['success'] = true;
                        $data['message'] = 'Email successfully sent.';
                        $data['alert_type'] = 'success';
                    }
                } else {
                    $data['message'] = 'Please complete the fields.';
                }
            } else {
                $data['message'] = 'Please complete the fields.';
            }
            echo json_encode($data);
        } else {
            redirect('login');
        }
    }

    function cancel_subscription(){
        if($this->ion_auth->logged_in()){
            $data = array();
            $data['success'] = false;
            $this->load->library("braintree_lib");
            $check_data = $this->braintree_lib->cancel_subscription();
            $update_data = array('braintree_subscription_status' => $check_data['subscription_status']);
            $this->UserModel->update_user($update_data);
            $data['success'] = $check_data['success'];
            echo json_encode($data);
        } else {
            redirect('login');
        }
    }

    function pause_subscription(){
        $user = $this->ion_auth->user()->row();
        if($this->ion_auth->logged_in()  && strtolower($user->is_paused) == 0){
            $data = array('success' => false);
            $this->load->library("braintree_lib");
            $check_data = $this->braintree_lib->pause_subscription();
            if($check_data['is_paused'] != ''){
                $check_data['is_paused'] = $check_data['is_paused'];
            } else {
                $check_data['is_paused'] = $user->is_paused;
            }
            $update_data = array('braintree_subscription_status' => $check_data['subscription_status'], 'is_paused' => $check_data['is_paused']);
            $this->UserModel->update_user($update_data);
            $data['success'] = $check_data['success'];
            echo json_encode($data);
        } else {
            redirect('login');
        }
    }

    function unpause_subscription(){
        $user = $this->ion_auth->user()->row();
        if($this->ion_auth->logged_in() && strtolower($user->is_paused) == 1){
            $data = array('success' => false);
            $this->load->library("braintree_lib");
            $check_data = $this->braintree_lib->unpause_subscription();
            if($check_data['is_paused'] == 0){
                $check_data['is_paused'] = $check_data['is_paused'];
            } else {
                $check_data['is_paused'] = $user->is_paused;
            }
            $update_data = array('braintree_subscription_status' => $check_data['subscription_status'], 'is_paused' => $check_data['is_paused']);
            $this->UserModel->update_user($update_data);
            $data['success'] = $check_data['success'];
            echo json_encode($data);
        } else {
            redirect('login');
        }
    }

    function confirm_cancel(){
        $update_data = array('active' => 0);
        $deactivate_user = $this->UserModel->update_user($update_data);
        if($deactivate_user){
            $this->ion_auth->logout();
            redirect('login');
        }
    }

    function submit_survey(){
        /*
        Array
        (
            [other_reason_to_cancel] => 
            [like_about_brunchwork] => 
            [improve_membership_exp] => 
            [perks_to_offer] => 
            [others] => 
        )
        */
        $data = array();
        $data['form'] = '';
        $data['success'] = false;
        if($this->input->post() && ($this->input->post('too_expensive') == '' && $this->input->post('time_constraints') == '' && $this->input->post('joined_other_org') == '' && $this->input->post('event_quality') == '' && $this->input->post('speaker_selection') == '') && $this->input->post('other_reason_to_cancel') == '' && $this->input->post('like_about_brunchwork') == '' && $this->input->post('improve_membership_exp') == '' && $this->input->post('perks_to_offer') == '' && $this->input->post('others') == ''){
            $data['form'] = 'empty';
        } else {
            $insert_data = $this->UserModel->insert_survey();
            if($insert_data){
                $data['success'] = true;
            }
        }
        echo json_encode($data);
    }
}